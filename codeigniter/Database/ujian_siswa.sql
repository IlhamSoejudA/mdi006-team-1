-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 17 Jul 2020 pada 08.33
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ujian_siswa`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `answers`
--

CREATE TABLE `answers` (
  `id` int(5) NOT NULL,
  `id_question` varchar(30) NOT NULL,
  `answer` text NOT NULL,
  `right_answer` int(5) NOT NULL,
  `image_url` varchar(70) NOT NULL,
  `video_url` varchar(70) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `is_deleted` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `answers`
--

INSERT INTO `answers` (`id`, `id_question`, `answer`, `right_answer`, `image_url`, `video_url`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, '1', 'jaawabannya adalah satu', 0, '7.png', '', '2020-06-30 10:28:32', '2020-07-12 03:42:47', '0'),
(2, '1', 'jawabannya adalah dua', 1, 'qweryui', 'asdfghjk', '2020-06-30 10:29:18', '2020-07-10 08:31:02', '0'),
(3, '2', 'jawabanya tidak ada yang benar', 1, 'asdfgh', 'qwert', '2020-06-30 11:04:20', '2020-07-10 08:31:05', '1'),
(4, '2', 'Jawaban salah semua', 0, 'asdfgh', 'asdf', '2020-07-10 16:15:09', '2020-07-10 09:16:03', '0'),
(5, '1', 'jawabannya sepuluh', 0, '8.png', '9.png', '2020-07-12 09:47:00', '2020-07-12 14:50:45', '0'),
(6, '1', 'jawabannya dua puluh', 0, '8.png', '6.png', '2020-07-12 09:50:37', NULL, '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `country` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `cities`
--

INSERT INTO `cities` (`id`, `name`, `country`) VALUES
(1, 'Bogotá', 'Colombia'),
(2, 'Medellín', 'Colombia'),
(3, 'Cali', 'Colombia'),
(4, 'Barranquilla', 'Colombia'),
(5, 'Cartagena', 'Colombia'),
(6, 'Bucaramanga', 'Colombia');

-- --------------------------------------------------------

--
-- Struktur dari tabel `events`
--

CREATE TABLE `events` (
  `id` int(5) NOT NULL,
  `event` varchar(70) NOT NULL,
  `id_exam` int(5) NOT NULL,
  `id_grade` int(5) NOT NULL,
  `id_majors` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `is_deleted` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `events`
--

INSERT INTO `events` (`id`, `event`, `id_exam`, `id_grade`, `id_majors`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 'UNBK Matematika SMA/SMK', 1, 3, 1, '2020-07-01 10:08:59', '2020-07-13 03:43:11', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `exams`
--

CREATE TABLE `exams` (
  `id` int(5) NOT NULL,
  `name_exam` varchar(70) NOT NULL,
  `question_total` int(5) NOT NULL,
  `status` varchar(50) NOT NULL,
  `duration` int(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `is_deleted` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `exams`
--

INSERT INTO `exams` (`id`, `name_exam`, `question_total`, `status`, `duration`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 'UNBK Matematika IPA 2018', 45, 'Draft', 90, '2020-07-09 11:06:17', '2020-07-12 03:44:50', '0'),
(2, 'UNBK Bahasa Indonesia 2016', 90, 'Draft', 120, '2020-07-09 11:07:50', NULL, '0'),
(3, 'UNBK Bahasa Inggris 2016', 90, 'Draft', 120, '2020-07-09 11:08:18', NULL, '0'),
(4, 'Ujian Nasional Matematika 2016', 90, 'Draft', 120, '2020-07-09 11:08:43', NULL, '0'),
(5, 'Ujian Nasional Bahasa Indonesia 2016', 90, 'Draft', 120, '2020-07-09 11:09:23', NULL, '0'),
(6, 'UNBK Matematika IPS 2017', 120, 'Draft', 60, '2020-07-10 12:18:43', NULL, '0'),
(7, 'UNBK matematika IPA 2017', 7, 'Draft', 90, '2020-07-10 01:11:28', NULL, '0'),
(8, 'UNBK  SMA Bahasa Inggris 2018', 120, 'Draft', 90, '2020-07-12 10:44:26', NULL, '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `grades`
--

CREATE TABLE `grades` (
  `id` int(5) NOT NULL,
  `grade` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `is_deleted` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `grades`
--

INSERT INTO `grades` (`id`, `grade`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 'SD', '2020-06-25 12:23:12', '2020-06-29 04:22:51', '0'),
(2, 'SMP', '2020-06-26 11:21:09', NULL, '0'),
(3, 'SMA / SMK', '2020-06-26 11:21:16', NULL, '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `majors`
--

CREATE TABLE `majors` (
  `id` int(5) NOT NULL,
  `major` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `is_deleted` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `majors`
--

INSERT INTO `majors` (`id`, `major`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 'Matematika', '2020-07-01 10:03:25', NULL, '0'),
(2, 'Bahasa Indonesia', '2020-07-01 10:03:25', NULL, '0'),
(3, 'IPA', '2020-07-01 10:03:25', '2020-07-01 05:04:28', '0'),
(4, 'Bahasa Inggris', '2020-07-01 10:03:25', '2020-07-01 05:03:39', '0'),
(5, 'Fisika', '2020-07-07 02:24:28', NULL, '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `questions`
--

CREATE TABLE `questions` (
  `id` int(5) NOT NULL,
  `id_exam` varchar(30) NOT NULL,
  `question` text NOT NULL,
  `image_url` varchar(70) DEFAULT NULL,
  `video_url` varchar(70) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `is_deleted` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `questions`
--

INSERT INTO `questions` (`id`, `id_exam`, `question`, `image_url`, `video_url`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, '1', 'berapa jumlah satu tambah satu', '7.png', '6.png', '2020-06-30 09:51:49', '2020-07-12 04:25:31', '0'),
(2, '1', 'berapa jumlah dua tambah dua', 'asdfghj', 'qwerty', '2020-06-30 09:55:37', '2020-07-09 04:56:04', '0'),
(3, '1', 'empaat tambah empat', 'cvbnm,', 'xcvbnm', '2020-06-30 10:02:43', '2020-07-09 04:56:06', '1'),
(4, '2', 'satuu', 'asd', 'asd', '2020-07-09 11:44:03', '2020-07-09 05:31:28', '0'),
(5, '3', 'duaa', 'adfs', 'egd', '2020-07-09 12:55:34', '2020-07-09 05:56:56', '0'),
(6, '4', 'dua tambah dua', 'asdd', 'asdasd', '2020-07-10 12:10:23', NULL, '0'),
(8, '1', 'berapa jumlah dua tambah limaaaaaa', '6.png', '3.png', '2020-07-12 10:53:22', NULL, '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `scoring`
--

CREATE TABLE `scoring` (
  `id_scoring` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_mapel` int(11) DEFAULT NULL,
  `id_jenjang` int(11) DEFAULT NULL,
  `paket_soal` varchar(45) DEFAULT NULL,
  `durasi_mengerjakan` varchar(45) DEFAULT NULL,
  `waktu_login` datetime DEFAULT NULL,
  `waktu_submit` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `level` varchar(45) NOT NULL,
  `email` varchar(150) DEFAULT NULL,
  `phone` varchar(150) DEFAULT NULL,
  `mobile` varchar(150) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `authorised` tinyint(1) DEFAULT 0,
  `block_expires` datetime DEFAULT NULL,
  `login_attempts` int(10) UNSIGNED DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `firstname`, `lastname`, `level`, `email`, `phone`, `mobile`, `address`, `country`, `city`, `birthday`, `authorised`, `block_expires`, `login_attempts`) VALUES
(1, 'admin', '$2y$10$QS.T0VwA5/a78y2s6m2Yiulu2bhGPzH/W1ay7QslX830PO4RTCVj.', 'Super', 'Admin', 'Superuser', 'my@email.com', '123123', '123123123', 'My address', 'Colombia', 'Bucaramanga', '1980-01-01', 1, NULL, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users_access`
--

CREATE TABLE `users_access` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `ip` varchar(250) DEFAULT NULL,
  `browser` text DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `domain` varchar(255) DEFAULT NULL,
  `country` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `users_access`
--

INSERT INTO `users_access` (`id`, `username`, `ip`, `browser`, `date`, `domain`, `country`) VALUES
(1, 'admin', '::1', 'PostmanRuntime/7.26.1', '2020-06-25 05:29:08', 'Feisal', 'XX'),
(2, 'admin', '::1', 'PostmanRuntime/7.26.1', '2020-07-02 06:56:21', 'Feisal', 'XX'),
(3, 'admin', '192.168.1.15', 'PostmanRuntime/7.26.1', '2020-07-07 06:18:15', 'fahuda_elwin_y', 'XX');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_exam`
--

CREATE TABLE `user_exam` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_exam`
--

INSERT INTO `user_exam` (`id`, `user_id`, `event_id`, `created_at`, `updated_at`) VALUES
(13, 2, 1, '2020-07-15 20:32:55', NULL),
(14, 2, 1, '2020-07-15 20:33:19', NULL),
(15, 2, 1, '2020-07-15 20:35:46', NULL),
(16, 3, 1, '2020-07-15 20:46:35', NULL),
(17, 2, 2, '2020-07-16 10:18:01', NULL),
(18, 3, 1, '2020-07-16 11:17:46', NULL),
(19, 4, 1, '2020-07-16 20:06:54', NULL),
(20, 4, 1, '2020-07-16 20:14:47', NULL),
(21, 4, 1, '2020-07-16 20:26:06', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_exam_answer`
--

CREATE TABLE `user_exam_answer` (
  `id` int(11) NOT NULL,
  `user_exam_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_exam_answer`
--

INSERT INTO `user_exam_answer` (`id`, `user_exam_id`, `question_id`, `answer_id`, `created_at`, `updated_at`) VALUES
(1, 17, 1, 2, '2020-07-16 10:43:18', NULL),
(2, 17, 1, 2, '2020-07-16 11:17:54', NULL),
(3, 19, 5, 8, '2020-07-16 20:27:38', NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `country` (`country`);

--
-- Indeks untuk tabel `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `exams`
--
ALTER TABLE `exams`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `majors`
--
ALTER TABLE `majors`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `scoring`
--
ALTER TABLE `scoring`
  ADD PRIMARY KEY (`id_scoring`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `username_2` (`username`),
  ADD KEY `email` (`email`(100)),
  ADD KEY `username` (`username`) USING BTREE,
  ADD KEY `level` (`level`);

--
-- Indeks untuk tabel `users_access`
--
ALTER TABLE `users_access`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_exam`
--
ALTER TABLE `user_exam`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_exam_answer`
--
ALTER TABLE `user_exam_answer`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `events`
--
ALTER TABLE `events`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `exams`
--
ALTER TABLE `exams`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `majors`
--
ALTER TABLE `majors`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `scoring`
--
ALTER TABLE `scoring`
  MODIFY `id_scoring` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `users_access`
--
ALTER TABLE `users_access`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `user_exam`
--
ALTER TABLE `user_exam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT untuk tabel `user_exam_answer`
--
ALTER TABLE `user_exam_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
