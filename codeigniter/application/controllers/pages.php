<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller
{   
    function __construct(){
		parent::__construct();		
		$this->load->library('user_agent');
 
	}
    public function index(){
        if ($this->agent->is_mobile())
        {
            //$this->load->view('mobile/landing_page');
        }
        else if ($this->agent->is_browser()){
            $this->load->view('templates/header');
                $data['title'] = 'Landing Page';
                $data['page'] = 'Landing Page';
                
            $this->load->view('pages/landing_page', $data);
            $this->load->view('templates/footer', $data);
		}
        else
        {
            $this->load->view('templates/header');
                $data['title'] = 'Landing Page';
                $data['page'] = 'Landing Page';
                
            $this->load->view('pages/landing_page', $data);
            $this->load->view('templates/footer', $data);
        }
 
		echo "Di akses dari :<br/>";
        echo "Platform = " . $this->agent->platform() ."<br/>";
        echo "IP = " . $this->input->ip_address()."<br/>";
        echo "Mobile = " . $this->agent->mobile()."<br/>";
        echo "Browser = " . $this->agent->browser()."<br/>";
    }
    public function fungsi(){
        echo "Function fungsi dari Controller Helloworld";
    }
    public function parameters($nama){
        echo "Selamat datang ".$nama;
    }
}