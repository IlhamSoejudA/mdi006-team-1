<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class List_ujian_sma extends CI_Controller
{   
    function __construct(){
		parent::__construct();		
		$this->load->library('user_agent');
    }
    
    public function index(){
    if ($this->agent->is_mobile())
    {
            //$this->load->view('mobile/list_ujian_smp');
    }
    else if ($this->agent->is_browser()){
        $this->load->model('sma');
        $data['list1'] = $this->sma->getUjian1();
        $data['list2'] = $this->sma->getUjian2();
        $this->load->view('templates/header');
            $data['title'] = 'List Ujian SMA';
            $data['page'] = 'List Ujian SMA';        
        $this->load->view('tryout/list_ujian_sma', $data);
        $this->load->view('templates/footer', $data);
    }
    else
    {
        $this->load->model('sma');
        $data['list1'] = $this->sma->getUjian1();
        $data['list2'] = $this->sma->getUjian2();
        $this->load->view('templates/header');
            $data['title'] = 'List Ujian SMA';
            $data['page'] = 'List Ujian SMA';        
        $this->load->view('tryout/list_ujian_sma', $data);
        $this->load->view('templates/footer', $data);
    }
    echo "Di akses dari :<br/>";
    echo "Sistem Operasi = " . $this->agent->platform() ."<br/>"; // Platform info (Windows, Linux, Mac, etc.)
    }

    public function fungsi(){
        echo "Function fungsi dari Controller Helloworld";
    }
    public function parameters($nama){
        echo "Selamat datang ".$nama;
    }
}