<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hasil_analisis extends CI_Controller
{   
    function __construct(){
		parent::__construct();		
		$this->load->library('user_agent');
    }
    public function index(){
        if ($this->agent->is_mobile())
        {
            //$this->load->view('mobile/hasil_analisis');
        }
        else if ($this->agent->is_browser()){
            $this->load->view('templates/header');
            $data['title'] = 'Analisis';
			$data['page'] = 'Analisis';
			$data['content'] = '
            o using Content here, content here, making it look like readable English.
            Many desktop publishing packages and web page editors now use Lorem Ipsum as 
            their default model text, and a search for lorem ipsum will uncover many web 
            sites still in their infancy. Various versions have evolved over the years, 
            sometimes by accident, sometimes on purpose (injected humour and the like).
								';
            $this->load->view('tryout/hasil_analisis', $data);
        }
        else{
            $this->load->view('templates/header');
            $data['title'] = 'Analisis';
			$data['page'] = 'Analisis';
			$data['content'] = '
            o using Content here, content here, making it look like readable English.
            Many desktop publishing packages and web page editors now use Lorem Ipsum as 
            their default model text, and a search for lorem ipsum will uncover many web 
            sites still in their infancy. Various versions have evolved over the years, 
            sometimes by accident, sometimes on purpose (injected humour and the like).
								';
            $this->load->view('tryout/hasil_analisis', $data);
        }
        echo "Di akses dari :<br/>";
        echo "Sistem Operasi = " . $this->agent->platform() ."<br/>"; // Platform info (Windows, Linux, Mac, etc.)
    }
    public function fungsi(){
        echo "Function fungsi dari Controller Helloworld";
    }
    public function parameters($nama){
        echo "Selamat datang ".$nama;
    }
}