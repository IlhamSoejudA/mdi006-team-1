<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mulai_ujian extends CI_Controller
{   
    function __construct(){
		parent::__construct();		
		$this->load->library('user_agent');
    }
    public function index(){
        if ($this->agent->is_mobile()){
            //$this->load->view('mobile/mulai_ujian');
        }
        else if ($this->agent->is_browser()){
            $this->load->view('templates/header');
            $data['title'] = 'Mulai Ujian';
			$data['page'] = 'Mulai Ujian';
			$data['content'] = '
								Many desktop publishing packages and web page editors now use Lorem Ipsum as 
								their default model text, and a search for lorem ipsum will uncover many web. 
								';
            $this->load->view('tryout/mulai_ujian', $data);
        }
        else{
            $this->load->view('templates/header');
            $data['title'] = 'Mulai Ujian';
			$data['page'] = 'Mulai Ujian';
			$data['content'] = '
								Many desktop publishing packages and web page editors now use Lorem Ipsum as 
								their default model text, and a search for lorem ipsum will uncover many web. 
								';
            $this->load->view('tryout/mulai_ujian', $data);
        }
        echo "Di akses dari :<br/>";
        echo "Sistem Operasi = " . $this->agent->platform() ."<br/>"; // Platform info (Windows, Linux, Mac, etc.)
    }
    public function fungsi(){
        echo "Function fungsi dari Controller Helloworld";
    }
    public function parameters($nama){
        echo "Selamat datang ".$nama;
    }
}