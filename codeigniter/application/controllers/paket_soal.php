<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paket_soal extends CI_Controller
{   
    function __construct(){
		parent::__construct();		
		$this->load->library('user_agent');
    }
    
    public function index(){
    if ($this->agent->is_mobile())
    {
            //$this->load->view('mobile/paket_soal');
    }
    else if ($this->agent->is_browser()){
        $this->load->model('start');
        $data['mapel1'] = $this->start->getIndonesia();
        $data['mapel2'] = $this->start->getIPA();
        $data['mapel3'] = $this->start->getMatematika();
        $data['mapel4'] = $this->start->getInggris();
        $data['mapel5'] = $this->start->getFisika();
        //grades
        $data['sd'] = $this->start->getSD();
        $data['smp'] = $this->start->getSMP();
        $data['sma'] = $this->start->getSMA();
        $this->load->view('templates/header');
            $data['title'] = 'Paket Soal';
            $data['page'] = 'Paket Soal';        
        $this->load->view('tryout/paket_soal', $data);
        $this->load->view('templates/footer', $data);
    }
    else
    {
        $this->load->model('start');
        $data['mapel1'] = $this->start->getIndonesia();
        $data['mapel2'] = $this->start->getIPA();
        $data['mapel3'] = $this->start->getMatematika();
        $data['mapel4'] = $this->start->getInggris();
        $data['mapel5'] = $this->start->getFisika();
        //grades
        $data['sd'] = $this->start->getSD();
        $data['smp'] = $this->start->getSMP();
        $data['sma'] = $this->start->getSMA();
        $this->load->view('templates/header');
            $data['title'] = 'Paket Soal';
            $data['page'] = 'Paket Soal';        
        $this->load->view('tryout/paket_soal', $data);
        $this->load->view('templates/footer', $data);
    }
    echo "Di akses dari :<br/>";
    echo "Sistem Operasi = " . $this->agent->platform() ."<br/>"; // Platform info (Windows, Linux, Mac, etc.)
    }

    public function fungsi(){
        echo "Function fungsi dari Controller Helloworld";
    }
    public function parameters($nama){
        echo "Selamat datang ".$nama;
    }
}