<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller
{   
    function __construct(){
		parent::__construct();		
		$this->load->library('user_agent');
    }
    
    public function index(){ 
        //view halaman Paket Soal
        $this->load->model('ps');
        $data['data'] = $this->ps->getListMapel();
        $this->load->view('templates/header');
            $data['title'] = 'Paket Soal';
            $data['page'] = 'Paket Soal';        
        $this->load->view('tryout/event', $data);
        $this->load->view('templates/footer', $data);
    }

    public function detail($gradeId, $majorId){
        //view halaman List Soal
        $this->load->model('ps');
        $data['data'] = $this->ps->getList($gradeId, $majorId);
        $data['id_grade'] = $gradeId;
        $data['id_major'] = $majorId;
        $this->load->view('templates/header');
            $data['title'] = 'List Ujian';
            $data['page'] = 'List Ujian';        
        $this->load->view('tryout/event/details', $data);
        $this->load->view('templates/footer', $data);
    }

    public function ujian($examsId){
         //view halaman Ujian
        $this->load->model('ujian');
        $data['data'] = $this->ujian->getUjian($examsId);
        $data['id_exams'] = $examsId;
        $this->load->model('siswa');
        $data['id'] = $this->siswa->getSiswa();
        $this->load->view('templates/header');
            $data['title'] = 'Ujian';
            $data['page'] = 'Ujian';        
        $this->load->view('tryout/event/ujian', $data);
        $this->load->view('templates/footer', $data);

        date_default_timezone_set('Asia/Jakarta');
        $datetime = date('Y-m-d H:i:s'); 

        $data = [
            'user_id' => 4,
            'event_id' => 1,
            'created_at' => $datetime
        ];
        $this->db->insert('user_exam', $data);

    }

    public function hasil($studentID, $examsId){
        //view halaman Hasil
        $this->load->model('hasil');
        $this->load->model('ujian');
        $data['data'] = $this->hasil->getHasil($studentID);
        $data['ujian'] = $this->ujian->getUjian($examsId);
        $data['id_student'] = $studentID;
        $data['id_exams'] = $examsId;
        $this->load->view('templates/header');
            $data['title'] = 'Hasil';
            $data['page'] = 'Hasil';        
        $this->load->view('tryout/event/hasil', $data);
        $this->load->view('templates/footer', $data);

        date_default_timezone_set('Asia/Jakarta');
        $datetime = date('Y-m-d H:i:s'); 

        $data = [
            'user_exam_id' => 19,
            'question_id' => 5,
            'answer_id' => 8,
            'created_at' => $datetime
        ];

        $this->db->insert('user_exam_answer', $data);
    }

    public function analisis($studentID, $examsId){
        //view halaman Analisis
        $this->load->model('analisis');
        $this->load->model('ujian');
        $data['data'] = $this->analisis->getAnalisis($studentID);
        $data['ujian'] = $this->ujian->getUjian($examsId);
        $data['id_student'] = $studentID;
        $data['id_exams'] = $examsId;
        $this->load->view('templates/header');
            $data['title'] = 'Analisis';
            $data['page'] = 'Analisis';        
        $this->load->view('tryout/event/analisis', $data);
        $this->load->view('templates/footer', $data);
    }

    public function pembahasan($examsId){
        //view halaman Pembahasan
        $this->load->model('pembahasan');
        $data['data'] = $this->pembahasan->getPembahasan($examsId);
        $data['id_exams'] = $examsId;
        $this->load->view('templates/header');
            $data['title'] = 'Pembahasan';
            $data['page'] = 'Pembahasan';        
        $this->load->view('tryout/event/pembahasan', $data);
        $this->load->view('templates/footer', $data);
   }
}