<?php 

use GuzzleHttp\Client;
use phpDocumentor\Reflection\Types\Array_;

class Ujian extends CI_model {

    public function getUjian($examsId){
        $data = Array([
            "exams_id" => 1,
            "grade" => "SD",
            "major" => "Bahasa Indonesia",
            "exams_name" => "UNBK Indonesia 2019 SD - A",
            "duration" => "90",
            "quest" => Array([
                "question_id" => 1,
                "category" => "C1",
                "question" => "Siapa aku",
                ],
                [
                "question_id" => 2,
                "category" => "C2",
                "question" => "Siapa Dia",
                ],
                [
                "question_id" => 3,
                "category" => "C3",
                "question" => "Siapa Mereka",
                ],
                [
                "question_id" => 4,
                "category" => "C4",
                "question" => "Siapa Kita",
                ],
                [
                "question_id" => 5,
                "category" => "C5",
                "question" => "Siapa aku",
                ],
                [
                "question_id" => 6,
                "category" => "C6",
                "question" => "Siapa Dia",
                ],
                [
                "question_id" => 7,
                "category" => "C6",
                "question" => "Siapa Mereka",
                ],
                [
                "question_id" => 8,
                "category" => "C5",
                "question" => "Siapa Kita",
                ],
                [
                "question_id" => 9,
                "category" => "C5",
                "question" => "Siapa Ya?",
                ],
                [
                "question_id" => 10,
                "category" => "C5",
                "question" => "Siapa Coba?",
                ],
                [
                "question_id" => 11,
                "category" => "C5",
                "question" => "Siapa aku",
                ],
                [
                "question_id" => 12,
                "category" => "C4",
                "question" => "Siapa Dia",
                ],
                [
                "question_id" => 13,
                "category" => "C4",
                "question" => "Siapa Mereka",
                ],
                [
                "question_id" => 14,
                "category" => "C4",
                "question" => "Siapa Kita",
                ],
                [
                "question_id" => 15,
                "category" => "C4",
                "question" => "Siapa aku",
                ],
                [
                "question_id" => 16,
                "category" => "C3",
                "question" => "Siapa Dia",
                ],
                [
                "question_id" => 17,
                "category" => "C4",
                "question" => "Siapa Mereka",
                ],
                [
                "question_id" => 18,
                "category" => "C4",
                "question" => "Siapa Kita",
                ],
                [
                "question_id" => 19,
                "category" => "C2",
                "question" => "Siapa Ya?",
                ],
                [
                "question_id" => 20,
                "category" => "C2",
                "question" => "Siapa Coba?",
                ],
                [
                "question_id" => 21,
                "category" => "C5",
                "question" => "Siapa aku",
                ],
                [
                "question_id" => 22,
                "category" => "C1",
                "question" => "Siapa Dia",
                ],
                [
                "question_id" => 23,
                "category" => "C1",
                "question" => "Siapa Mereka",
                ],
                [
                "question_id" => 24,
                "category" => "C6",
                "question" => "Siapa Kita",
                ],
                [
                "question_id" => 25,
                "category" => "C6",
                "question" => "Siapa aku",
                ],
            ),
            "ans" => Array([
                "answer_id" => 1,
                "data_answers" => Array([
                    "ans" => "Aku A1"
                    ],
                    [
                    "ans" => "Aku B1"
                    ],
                    [
                    "ans" => "Aku C1"
                    ],
                    [
                    "ans" => "Aku D1"
                    ],),
                ],
                [
                "answer_id" => 2,
                "data_answers" => Array([
                    "ans" => "Aku A2"
                    ],
                    [
                    "ans" => "Aku B2"
                    ],
                    [
                    "ans" => "Aku C2"
                    ],
                    [
                    "ans" => "Aku D2"
                    ],),
                ],
                [
                "answer_id" => 3,
                "data_answers" => Array([
                    "ans" => "Aku A3"
                    ],
                    [
                    "ans" => "Aku B3"
                    ],
                    [
                    "ans" => "Aku C3"
                    ],
                    [
                    "ans" => "Aku D3"
                    ],),
                ],
                [
                "answer_id" => 4,
                "data_answers" => Array([
                    "ans" => "Aku A4"
                    ],
                    [
                    "ans" => "Aku B4"
                    ],
                    [
                    "ans" => "Aku C4"
                    ],
                    [
                    "ans" => "Aku D4"
                    ],),
                ],
                 [
                "answer_id" => 5,
                "data_answers" => Array([
                    "ans" => "Aku A5"
                    ],
                    [
                    "ans" => "Aku B5"
                    ],
                    [
                    "ans" => "Aku C5"
                    ],
                    [
                    "ans" => "Aku D5"
                    ],),
                ],
                 [
                "answer_id" => 6,
                "data_answers" => Array([
                    "ans" => "Aku A6"
                    ],
                    [
                    "ans" => "Aku B6"
                    ],
                    [
                    "ans" => "Aku C6"
                    ],
                    [
                    "ans" => "Aku D6"
                    ],),
                ],
                 [
                "answer_id" => 7,
                "data_answers" => Array([
                    "ans" => "Aku A7"
                    ],
                    [
                    "ans" => "Aku B7"
                    ],
                    [
                    "ans" => "Aku C7"
                    ],
                    [
                    "ans" => "Aku D7"
                    ],),
                ],
                 [
                "answer_id" => 8,
                "data_answers" => Array([
                    "ans" => "Aku A8"
                    ],
                    [
                    "ans" => "Aku B8"
                    ],
                    [
                    "ans" => "Aku C8"
                    ],
                    [
                    "ans" => "Aku D8"
                    ],),
                ],
                 [
                "answer_id" => 9,
                "data_answers" => Array([
                    "ans" => "Aku A9"
                    ],
                    [
                    "ans" => "Aku B9"
                    ],
                    [
                    "ans" => "Aku C9"
                    ],
                    [
                    "ans" => "Aku D9"
                    ],),
                ],
                 [
                "answer_id" => 10,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],),
                ],
                 [
                "answer_id" => 11,
                "data_answers" => Array([
                    "ans" => "Aku A11"
                    ],
                    [
                    "ans" => "Aku B11"
                    ],
                    [
                    "ans" => "Aku C11"
                    ],
                    [
                    "ans" => "Aku D11"
                    ],),
                ],
                 [
                "answer_id" => 12,
                "data_answers" => Array([
                    "ans" => "Aku A12"
                    ],
                    [
                    "ans" => "Aku B12"
                    ],
                    [
                    "ans" => "Aku C12"
                    ],
                    [
                    "ans" => "Aku D12"
                    ],),
                ],
                 [
                "answer_id" => 13,
                "data_answers" => Array([
                    "ans" => "Aku A13"
                    ],
                    [
                    "ans" => "Aku B13"
                    ],
                    [
                    "ans" => "Aku C13"
                    ],
                    [
                    "ans" => "Aku D13"
                    ],),
                ],
                 [
                "answer_id" => 14,
                "data_answers" => Array([
                    "ans" => "Aku A14"
                    ],
                    [
                    "ans" => "Aku B14"
                    ],
                    [
                    "ans" => "Aku C14"
                    ],
                    [
                    "ans" => "Aku D14"
                    ],),
                ],
                 [
                "answer_id" => 15,
                "data_answers" => Array([
                    "ans" => "Aku A15"
                    ],
                    [
                    "ans" => "Aku B15"
                    ],
                    [
                    "ans" => "Aku C15"
                    ],
                    [
                    "ans" => "Aku D15"
                    ],),
                ],
                 [
                "answer_id" => 16,
                "data_answers" => Array([
                    "ans" => "Aku A16"
                    ],
                    [
                    "ans" => "Aku B16"
                    ],
                    [
                    "ans" => "Aku C16"
                    ],
                    [
                    "ans" => "Aku D16"
                    ],),
                ],
                 [
                "answer_id" => 17,
                "data_answers" => Array([
                    "ans" => "Aku A17"
                    ],
                    [
                    "ans" => "Aku B17"
                    ],
                    [
                    "ans" => "Aku C17"
                    ],
                    [
                    "ans" => "Aku D17"
                    ],),
                ],
                 [
                "answer_id" => 18,
                "data_answers" => Array([
                    "ans" => "Aku A18"
                    ],
                    [
                    "ans" => "Aku B18"
                    ],
                    [
                    "ans" => "Aku C18"
                    ],
                    [
                    "ans" => "Aku D18"
                    ],),
                ],
                 [
                "answer_id" => 19,
                "data_answers" => Array([
                    "ans" => "Aku A19"
                    ],
                    [
                    "ans" => "Aku B19"
                    ],
                    [
                    "ans" => "Aku C19"
                    ],
                    [
                    "ans" => "Aku D19"
                    ],),
                ],
                 [
                "answer_id" => 20,
                "data_answers" => Array([
                    "ans" => "Aku A20"
                    ],
                    [
                    "ans" => "Aku B20"
                    ],
                    [
                    "ans" => "Aku C20"
                    ],
                    [
                    "ans" => "Aku D20"
                    ],),
                ],
                 [
                "answer_id" => 21,
                "data_answers" => Array([
                    "ans" => "Aku A21"
                    ],
                    [
                    "ans" => "Aku B21"
                    ],
                    [
                    "ans" => "Aku C21"
                    ],
                    [
                    "ans" => "Aku D21"
                    ],),
                ],
                 [
                "answer_id" => 22,
                "data_answers" => Array([
                    "ans" => "Aku A22"
                    ],
                    [
                    "ans" => "Aku B22"
                    ],
                    [
                    "ans" => "Aku C22"
                    ],
                    [
                    "ans" => "Aku D22"
                    ],),
                ],
                 [
                "answer_id" => 23,
                "data_answers" => Array([
                    "ans" => "Aku A23"
                    ],
                    [
                    "ans" => "Aku B23"
                    ],
                    [
                    "ans" => "Aku C23"
                    ],
                    [
                    "ans" => "Aku D23"
                    ],),
                ],
                 [
                "answer_id" => 24,
                "data_answers" => Array([
                    "ans" => "Aku 24"
                    ],
                    [
                    "ans" => "Aku B24"
                    ],
                    [
                    "ans" => "Aku C24"
                    ],
                    [
                    "ans" => "Aku D24"
                    ],),
                ],
                [
                "answer_id" => 25,
                "data_answers" => Array([
                    "ans" => "Aku 25"
                    ],
                    [
                    "ans" => "Aku B25"
                    ],
                    [
                    "ans" => "Aku C25"
                    ],
                    [
                    "ans" => "Aku D25"
                    ],),
                ],
            ),                      
        ],
        [
            "exams_id" => 13,
            "exams_name" => "UNBK Indonesia 2019 SMP - A",
            "grade" => "SMP",
            "major" => "Bahasa Indonesia",
            "duration" => "120",
            "quest" => Array([
                "question_id" => 1,
                "category" => "C1",
                "question" => "Siapa aku",
                ],
                [
                "question_id" => 2,
                "category" => "C2",
                "question" => "Siapa Dia",
                ],
                [
                "question_id" => 3,
                "category" => "C3",
                "question" => "Siapa Mereka",
                ],
                [
                "question_id" => 4,
                "category" => "C4",
                "question" => "Siapa Kita",
                ],
                [
                "question_id" => 5,
                "category" => "C5",
                "question" => "Siapa aku",
                ],
                [
                "question_id" => 6,
                "category" => "C6",
                "question" => "Siapa Dia",
                ],
                [
                "question_id" => 7,
                "category" => "C6",
                "question" => "Siapa Mereka",
                ],
                [
                "question_id" => 8,
                "category" => "C5",
                "question" => "Siapa Kita",
                ],
                [
                "question_id" => 9,
                "category" => "C5",
                "question" => "Siapa Ya?",
                ],
                [
                "question_id" => 10,
                "category" => "C5",
                "question" => "Siapa Coba?",
                ],
                [
                "question_id" => 11,
                "category" => "C5",
                "question" => "Siapa aku",
                ],
                [
                "question_id" => 12,
                "category" => "C4",
                "question" => "Siapa Dia",
                ],
                [
                "question_id" => 13,
                "category" => "C4",
                "question" => "Siapa Mereka",
                ],
                [
                "question_id" => 14,
                "category" => "C4",
                "question" => "Siapa Kita",
                ],
                [
                "question_id" => 15,
                "category" => "C4",
                "question" => "Siapa aku",
                ],
                [
                "question_id" => 16,
                "category" => "C3",
                "question" => "Siapa Dia",
                ],
                [
                "question_id" => 17,
                "category" => "C4",
                "question" => "Siapa Mereka",
                ],
                [
                "question_id" => 18,
                "category" => "C4",
                "question" => "Siapa Kita",
                ],
                [
                "question_id" => 19,
                "category" => "C2",
                "question" => "Siapa Ya?",
                ],
                [
                "question_id" => 20,
                "category" => "C2",
                "question" => "Siapa Coba?",
                ],
                [
                "question_id" => 21,
                "category" => "C5",
                "question" => "Siapa aku",
                ],
                [
                "question_id" => 22,
                "category" => "C1",
                "question" => "Siapa Dia",
                ],
                [
                "question_id" => 23,
                "category" => "C1",
                "question" => "Siapa Mereka",
                ],
                [
                "question_id" => 24,
                "category" => "C6",
                "question" => "Siapa Kita",
                ],
                [
                "question_id" => 25,
                "category" => "C6",
                "question" => "Siapa aku",
                ],
                [
                "question_id" => 26,
                "category" => "C5",
                "question" => "Siapa Dia",
                ],
                [
                "question_id" => 27,
                "category" => "C3",
                "question" => "Siapa Mereka",
                ],
                [
                "question_id" => 28,
                "category" => "C2",
                "question" => "Siapa Kita",
                ],
                [
                "question_id" => 29,
                "category" => "C1",
                "question" => "Siapa Ya?",
                ],
                [
                "question_id" => 30,
                "category" => "C1",
                "question" => "Siapa Coba?",
                ],
                [
                "question_id" => 31,
                "category" => "C2",
                "question" => "Siapa aku",
                ],
                [
                "question_id" => 32,
                "category" => "C1",
                "question" => "Siapa Dia",
                ],
                [
                "question_id" => 33,
                "category" => "C4",
                "question" => "Siapa Mereka",
                ],
                [
                "question_id" => 34,
                "category" => "C3",
                "question" => "Siapa Kita",
                ],
                [
                "question_id" => 35,
                "category" => "C3",
                "question" => "Siapa aku",
                ],
                [
                "question_id" => 36,
                "category" => "C3",
                "question" => "Siapa Dia",
                ],
                [
                "question_id" => 37,
                "category" => "C2",
                "question" => "Siapa Mereka",
                ],
                [
                "question_id" => 38,
                "category" => "C2",
                "question" => "Siapa Kita",
                ],
                [
                "question_id" => 39,
                "category" => "C2",
                "question" => "Siapa Ya?",
                ],
                [
                "question_id" => 40,
                "category" => "C5",
                "question" => "Siapa Coba?",
                ],
                [
                "question_id" => 41,
                "category" => "C5",
                "question" => "Siapa aku",
                ],
                [
                "question_id" => 42,
                "category" => "C1",
                "question" => "Siapa Dia",
                ],
                [
                "question_id" => 43,
                "category" => "C1",
                "question" => "Siapa Mereka",
                ],
                [
                "question_id" => 44,
                "category" => "C6",
                "question" => "Siapa Kita",
                ],
                [
                "question_id" => 45,
                "category" => "C6",
                "question" => "Siapa aku",
                ],
                [
                "question_id" => 46,
                "category" => "C5",
                "question" => "Siapa Dia",
                ],
                [
                "question_id" => 47,
                "category" => "C2",
                "question" => "Siapa Mereka",
                ],
                [
                "question_id" => 48,
                "category" => "C2",
                "question" => "Siapa Kita",
                ],
                [
                "question_id" => 49,
                "category" => "C2",
                "question" => "Siapa Ya?",
                ],
                [
                "question_id" => 50,
                "category" => "C1",
                "question" => "Siapa Coba?",
                ],                
            ),
            "ans" => Array([
                "answer_id" => 1,
                "data_answers" => Array([
                    "ans" => "Aku A1"
                    ],
                    [
                    "ans" => "Aku B1"
                    ],
                    [
                    "ans" => "Aku C1"
                    ],
                    [
                    "ans" => "Aku D1"
                    ],
                    [
                    "ans" => "Aku E1"
                    ]),
                ],
                [
                "answer_id" => 2,
                "data_answers" => Array([
                    "ans" => "Aku A2"
                    ],
                    [
                    "ans" => "Aku B2"
                    ],
                    [
                    "ans" => "Aku C2"
                    ],
                    [
                    "ans" => "Aku D2"
                    ],
                    [
                    "ans" => "Aku E2"
                    ]),
                ],
                [
                "answer_id" => 3,
                "data_answers" => Array([
                    "ans" => "Aku A3"
                    ],
                    [
                    "ans" => "Aku B3"
                    ],
                    [
                    "ans" => "Aku C3"
                    ],
                    [
                    "ans" => "Aku D3"
                    ],
                    [
                    "ans" => "Aku E3"
                    ]),
                ],
                [
                "answer_id" => 4,
                "data_answers" => Array([
                    "ans" => "Aku A4"
                    ],
                    [
                    "ans" => "Aku B4"
                    ],
                    [
                    "ans" => "Aku C4"
                    ],
                    [
                    "ans" => "Aku D4"
                    ],
                    [
                    "ans" => "Aku E4"
                    ]),
                ],
                 [
                "answer_id" => 5,
                "data_answers" => Array([
                    "ans" => "Aku A5"
                    ],
                    [
                    "ans" => "Aku B5"
                    ],
                    [
                    "ans" => "Aku C5"
                    ],
                    [
                    "ans" => "Aku D5"
                    ],
                    [
                    "ans" => "Aku E5"
                    ]),
                ],
                 [
                "answer_id" => 6,
                "data_answers" => Array([
                    "ans" => "Aku A6"
                    ],
                    [
                    "ans" => "Aku B6"
                    ],
                    [
                    "ans" => "Aku C6"
                    ],
                    [
                    "ans" => "Aku D6"
                    ],
                    [
                    "ans" => "Aku E6"
                    ]),
                ],
                 [
                "answer_id" => 7,
                "data_answers" => Array([
                    "ans" => "Aku A7"
                    ],
                    [
                    "ans" => "Aku B7"
                    ],
                    [
                    "ans" => "Aku C7"
                    ],
                    [
                    "ans" => "Aku D7"
                    ],
                    [
                    "ans" => "Aku E7"
                    ]),
                ],
                 [
                "answer_id" => 8,
                "data_answers" => Array([
                    "ans" => "Aku A8"
                    ],
                    [
                    "ans" => "Aku B8"
                    ],
                    [
                    "ans" => "Aku C8"
                    ],
                    [
                    "ans" => "Aku D8"
                    ],
                    [
                    "ans" => "Aku E8"
                    ]),
                ],
                 [
                "answer_id" => 9,
                "data_answers" => Array([
                    "ans" => "Aku A9"
                    ],
                    [
                    "ans" => "Aku B9"
                    ],
                    [
                    "ans" => "Aku C9"
                    ],
                    [
                    "ans" => "Aku D9"
                    ],
                    [
                    "ans" => "Aku E9"
                    ]),
                ],
                 [
                "answer_id" => 10,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 11,
                "data_answers" => Array([
                    "ans" => "Aku A11"
                    ],
                    [
                    "ans" => "Aku B11"
                    ],
                    [
                    "ans" => "Aku C11"
                    ],
                    [
                    "ans" => "Aku D11"
                    ],
                    [
                    "ans" => "Aku E11"
                    ]),
                ],
                 [
                "answer_id" => 12,
                "data_answers" => Array([
                    "ans" => "Aku A12"
                    ],
                    [
                    "ans" => "Aku B12"
                    ],
                    [
                    "ans" => "Aku C12"
                    ],
                    [
                    "ans" => "Aku D12"
                    ],
                    [
                    "ans" => "Aku E12"
                    ]),
                ],
                 [
                "answer_id" => 13,
                "data_answers" => Array([
                    "ans" => "Aku A13"
                    ],
                    [
                    "ans" => "Aku B13"
                    ],
                    [
                    "ans" => "Aku C13"
                    ],
                    [
                    "ans" => "Aku D13"
                    ],
                    [
                    "ans" => "Aku E13"
                    ]),
                ],
                 [
                "answer_id" => 14,
                "data_answers" => Array([
                    "ans" => "Aku A14"
                    ],
                    [
                    "ans" => "Aku B14"
                    ],
                    [
                    "ans" => "Aku C14"
                    ],
                    [
                    "ans" => "Aku D14"
                    ],
                    [
                    "ans" => "Aku E14"
                    ]),
                ],
                 [
                "answer_id" => 15,
                "data_answers" => Array([
                    "ans" => "Aku A15"
                    ],
                    [
                    "ans" => "Aku B15"
                    ],
                    [
                    "ans" => "Aku C15"
                    ],
                    [
                    "ans" => "Aku D15"
                    ],
                    [
                    "ans" => "Aku E15"
                    ]),
                ],
                 [
                "answer_id" => 16,
                "data_answers" => Array([
                    "ans" => "Aku A16"
                    ],
                    [
                    "ans" => "Aku B16"
                    ],
                    [
                    "ans" => "Aku C16"
                    ],
                    [
                    "ans" => "Aku D16"
                    ],
                    [
                    "ans" => "Aku E16"
                    ]),
                ],
                 [
                "answer_id" => 17,
                "data_answers" => Array([
                    "ans" => "Aku A17"
                    ],
                    [
                    "ans" => "Aku B17"
                    ],
                    [
                    "ans" => "Aku C17"
                    ],
                    [
                    "ans" => "Aku D17"
                    ],
                    [
                    "ans" => "Aku E17"
                    ]),
                ],
                 [
                "answer_id" => 18,
                "data_answers" => Array([
                    "ans" => "Aku A18"
                    ],
                    [
                    "ans" => "Aku B18"
                    ],
                    [
                    "ans" => "Aku C18"
                    ],
                    [
                    "ans" => "Aku D18"
                    ],
                    [
                    "ans" => "Aku E18"
                    ]),
                ],
                 [
                "answer_id" => 19,
                "data_answers" => Array([
                    "ans" => "Aku A19"
                    ],
                    [
                    "ans" => "Aku B19"
                    ],
                    [
                    "ans" => "Aku C19"
                    ],
                    [
                    "ans" => "Aku D19"
                    ],
                    [
                    "ans" => "Aku E19"
                    ]),
                ],
                 [
                "answer_id" => 20,
                "data_answers" => Array([
                    "ans" => "Aku A20"
                    ],
                    [
                    "ans" => "Aku B20"
                    ],
                    [
                    "ans" => "Aku C20"
                    ],
                    [
                    "ans" => "Aku D20"
                    ],
                    [
                    "ans" => "Aku E20"
                    ]),
                ],
                 [
                "answer_id" => 21,
                "data_answers" => Array([
                    "ans" => "Aku A21"
                    ],
                    [
                    "ans" => "Aku B21"
                    ],
                    [
                    "ans" => "Aku C21"
                    ],
                    [
                    "ans" => "Aku D21"
                    ],
                    [
                    "ans" => "Aku E21"
                    ]),
                ],
                 [
                "answer_id" => 22,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 23,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 24,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 25,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 26,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 27,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 28,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 29,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 30,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 31,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 32,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 33,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 34,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 35,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 36,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 37,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 38,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 39,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 40,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 41,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 42,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 43,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 44,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 45,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 46,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 47,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 48,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 49,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 50,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 10,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 10,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 10,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
                 [
                "answer_id" => 10,
                "data_answers" => Array([
                    "ans" => "Aku A10"
                    ],
                    [
                    "ans" => "Aku B10"
                    ],
                    [
                    "ans" => "Aku C10"
                    ],
                    [
                    "ans" => "Aku D10"
                    ],
                    [
                    "ans" => "Aku E10"
                    ]),
                ],
            ),                      
        ]
    );
        return $data;
    }
}