<?php 

use GuzzleHttp\Client;
use phpDocumentor\Reflection\Types\Array_;

class Hasil extends CI_model {

    public function getHasil($studentID){
        $data = Array([
            "id_siswa" => 1,
            "nama_siswa" => "Andrea",
            "mata_pelajaran" => "Bahasa Indonesia",
            "paket_soal" => "UNBK Indonesia 2019 SD - A",
            "waktu_pengerjaan" => "30:25",
            "jawaban_benar" => "10",
            "jawaban_salah" => "15",
            ],
            [
            "id_siswa" => 2,
            "nama_siswa" => "Silvia",
            "mata_pelajaran" => "Bahasa Indonesia",
            "paket_soal" => "UNBK Indonesia 2019 SMP - A",
            "waktu_pengerjaan" => "01:20:34",
            "jawaban_benar" => "40",
            "jawaban_salah" => "10",
            ]                    
        );
        return $data;
    }
} 