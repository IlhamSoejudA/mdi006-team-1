<?php 

use GuzzleHttp\Client;
use phpDocumentor\Reflection\Types\Array_;

class Ps extends CI_model {

    public function getListMapel(){
        $data = Array([
            "id_grade" => 1,
            "grade" => "SD",
            "majors" => Array([
                "majors_id" => 1,
                "majors_name" => "Bahasa Indonesia",
                "exams" => Array([
                    "exams_id" => 1,
                    "exams_name" => "UNBK Indonesia 2019 SD - A",
                    "total" => "25",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 2,
                    "exams_name" => "UNBK Indonesia 2019 SD - B",
                    "total" => "50",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 3,
                    "exams_name" => "UNBK Indonesia 2019 SD - C",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 4,
                    "exams_name" => "UNBK Indonesia 2019 SD - D",
                    "total" => "50",
                    "duration" => "90",
                    ])
                ],
                [
                "majors_id" => 2,
                "majors_name" => "IPA",
                "exams" => Array([
                    "exams_id" => 5,
                    "exams_name" => "UNBK IPA 2019 SD - A",
                    "total" => "50",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 6,
                    "exams_name" => "UNBK IPA 2019 SD - B",
                    "total" => "50",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 7,
                    "exams_name" => "UNBK IPA 2019 SD - C",
                    "total" => "50",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 8,
                    "exams_name" => "UNBK IPA 2019 SD - D",
                    "total" => "50",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 9,
                    "exams_name" => "UNBK IPA 2019 SD - E",
                    "total" => "50",
                    "duration" => "120",
                    ])
                ],
                [
                "majors_id" => 3,
                "majors_name" => "Matematika",
                "exams" => Array([
                    "exams_id" => 10,
                    "exams_name" => "UNBK Matematika 2019 SD - A",
                    "total" => "50",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 11,
                    "exams_name" => "UNBK Matematika 2019 SD - B",
                    "total" => "50",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 12,
                    "exams_name" => "UNBK Matematika 2019 SD - C",
                    "total" => "50",
                    "duration" => "120",
                    ])
                ]),
            ], 
            [
            "id_grade" => 2,
            "grade" => "SMP",
            "majors" => Array([
                "majors_id" => 1,
                "majors_name" => "Bahasa Indonesia",
                "exams" => Array([
                    "exams_id" => 13,
                    "exams_name" => "UNBK 2019 Indonesia SMP - A",
                    "total" => "40",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 14,
                    "exams_name" => "UNBK 2019 Indonesia SMP - B",
                    "total" => "40",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 15,
                    "exams_name" => "UNBK 2019 Indonesia SMP - C",
                    "total" => "40",
                    "duration" => "120",
                    ])
                ],
                [
                "majors_id" => 2,
                "majors_name" => "IPA",
                "exams" => Array([
                    "exams_id" => 16,
                    "exams_name" => "UNBK 2019 IPA SMP - A",
                    "total" => "40",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 17,
                    "exams_name" => "UNBK 2019 IPA SMP - B",
                    "total" => "40",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 18,
                    "exams_name" => "UNBK 2019 IPA SMP - C",
                    "total" => "40",
                    "duration" => "120",
                    ])
                ],
                [
                "majors_id" => 3,
                "majors_name" => "Matematika",
                "exams" => Array([
                    "exams_id" => 19,
                    "exams_name" => "UNBK 2019 SMP Matematika - A",
                    "total" => "40",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 20,
                    "exams_name" => "UNBK 2019 SMP Matematika - B",
                    "total" => "40",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 21,
                    "exams_name" => "UNBK 2019 SMP Matematika - C",
                    "total" => "40",
                    "duration" => "120",
                    ])
                ],
                [
                "majors_id" => 4,
                "majors_name" => "Bahasa Inggris",
                "exams" => Array([
                    "exams_id" => 22,
                    "exams_name" => "UNBK 2019 Inggris SMP - A",
                    "total" => "40",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 23,
                    "exams_name" => "UNBK 2019 Inggris SMP - B",
                    "total" => "40",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 24,
                    "exams_name" => "UNBK 2019 Inggris SMP - C",
                    "total" => "40",
                    "duration" => "120",
                    ])
                ]),
            ],
            [
            "id_grade" => 3,
            "grade" => "SMA",
            "majors" => Array([
                "majors_id" => 1,
                "majors_name" => "Bahasa Indonesia",
                "exams" => Array([
                    "exams_id" => 25,
                    "exams_name" => "UNBK 2019 Indonesia SMA - A",
                    "total" => "50",
                    "duration" => "130",
                    ],
                    [
                    "exams_id" => 26,
                    "exams_name" => "UNBK 2019 Indonesia SMA - B",
                    "total" => "50",
                    "duration" => "130",
                    ],
                    [
                    "exams_id" => 27,
                    "exams_name" => "UNBK 2019 Indonesia SMA - C",
                    "total" => "50",
                    "duration" => "130",
                    ])
                ],
                [
                "majors_id" => 3,
                "majors_name" => "Matematika",
                "exams" => Array([
                    "exams_id" => 28,
                    "exams_name" => "UNBK 2019 Matematika SMA - A",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 29,
                    "exams_name" => "UNBK 2019 Matematika SMA - B",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 30,
                    "exams_name" => "UNBK 2019 Matematika SMA - C",
                    "total" => "50",
                    "duration" => "120",
                    ])
                ],
                [
                "majors_id" => 5,
                "majors_name" => "Fisika",
                "exams" => Array([
                    "exams_id" => 31,
                    "exams_name" => "UNBK 2019 Fisika SMA - A",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 32,
                    "exams_name" => "UNBK 2019 Fisika SMA - B",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 33,
                    "exams_name" => "UNBK 2019 Fisika SMA - C",
                    "total" => "50",
                    "duration" => "120",
                    ])
                ],
                [
                "majors_id" => 6,
                "majors_name" => "Biologi",
                "exams" => Array([
                    "exams_id" => 34,
                    "exams_name" => "UNBK 2019 Biologi SMA - A",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 35,
                    "exams_name" => "UNBK 2019 Biologi SMA - B",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 36,
                    "exams_name" => "UNBK 2019 Biologi SMA - C",
                    "total" => "50",
                    "duration" => "120",
                    ])
                ],
                [
                "majors_id" => 7,
                "majors_name" => "Kimia",
                "exams" => Array([
                    "exams_id" => 37,
                    "exams_name" => "UNBK 2019 Kimia SMA - A",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 38,
                    "exams_name" => "UNBK 2019 Kimia SMA - B",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 39,
                    "exams_name" => "UNBK 2019 Kimia SMA - C",
                    "total" => "50",
                    "duration" => "120",
                    ])
                ],
                [
                "majors_id" => 4,
                "majors_name" => "Bahasa Inggris",
                "exams" => Array([
                    "exams_id" => 40,
                    "exams_name" => "UNBK 2019 Inggris SMA - A",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 41,
                    "exams_name" => "UNBK 2019 Inggris SMA - B",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 42,
                    "exams_name" => "UNBK 2019 Inggris SMA - C",
                    "total" => "50",
                    "duration" => "120",
                    ])
                ],
                [
                "majors_id" => 8,
                "majors_name" => "Geografi",
                "exams" => Array([
                    "exams_id" => 43,
                    "exams_name" => "UNBK 2019 Geografi SMA - A",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 44,
                    "exams_name" => "UNBK 2019 Geografi SMA - B",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 45,
                    "exams_name" => "UNBK 2019 Geografi SMA - C",
                    "total" => "50",
                    "duration" => "120",
                    ])
                ],
                [
                "majors_id" => 9,
                "majors_name" => "Ekonomi",
                "exams" => Array([
                    "exams_id" => 46,
                    "exams_name" => "UNBK Ekonomi 2019 SMA - A",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 47,
                    "exams_name" => "UNBK Ekonomi 2019 SMA - B",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 48,
                    "exams_name" => "UNBK Ekonomi 2019 SMA - C",
                    "total" => "50",
                    "duration" => "120",
                    ])
                ],
                [
                "majors_id" => 10,
                "majors_name" => "Sosiologi",
                "exams" => Array([
                    "exams_id" => 49,
                    "exams_name" => "UNBK Sosiologi 2019 SMA - A",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 50,
                    "exams_name" => "UNBK Sosiologi 2019 SMA - B",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 51,
                    "exams_name" => "UNBK Sosiologi 2019 SMA - C",
                    "total" => "50",
                    "duration" => "120",
                    ])
                ]),
            ],                           
        );
        return $data;
    }
    public function getList($gradeId, $majorId){
        $data = Array([
            "id_grade" => 1,
            "grade" => "SD",
            "majors" => Array([
                "majors_id" => 1,
                "majors_name" => "Bahasa Indonesia",
                "exams" => Array([
                    "exams_id" => 1,
                    "exams_name" => "UNBK Indonesia 2019 SD - A",
                    "total" => "25",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 2,
                    "exams_name" => "UNBK Indonesia 2019 SD - B",
                    "total" => "50",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 3,
                    "exams_name" => "UNBK Indonesia 2019 SD - C",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 4,
                    "exams_name" => "UNBK Indonesia 2019 SD - D",
                    "total" => "50",
                    "duration" => "90",
                    ])
                ],
                [
                "majors_id" => 2,
                "majors_name" => "IPA",
                "exams" => Array([
                    "exams_id" => 5,
                    "exams_name" => "UNBK IPA 2019 - A",
                    "total" => "50",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 6,
                    "exams_name" => "UNBK IPA 2019 - B",
                    "total" => "50",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 7,
                    "exams_name" => "UNBK IPA 2019 - C",
                    "total" => "50",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 8,
                    "exams_name" => "UNBK IPA 2019 - D",
                    "total" => "50",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 9,
                    "exams_name" => "UNBK IPA 2019 - E",
                    "total" => "50",
                    "duration" => "120",
                    ])
                ],
                [
                "majors_id" => 3,
                "majors_name" => "Matematika",
                "exams" => Array([
                    "exams_id" => 10,
                    "exams_name" => "UNBK Matematika 2019 SD - A",
                    "total" => "50",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 11,
                    "exams_name" => "UNBK Matematika 2019 SD - B",
                    "total" => "50",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 12,
                    "exams_name" => "UNBK Matematika 2019 SD - C",
                    "total" => "50",
                    "duration" => "120",
                    ])
                ]),
            ], 
            [
            "id_grade" => 2,
            "grade" => "SMP",
            "majors" => Array([
                "majors_id" => 1,
                "majors_name" => "Bahasa Indonesia",
                "exams" => Array([
                    "exams_id" => 13,
                    "exams_name" => "UNBK 2019 Indonesia SMP - A",
                    "total" => "40",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 14,
                    "exams_name" => "UNBK 2019 Indonesia SMP - B",
                    "total" => "40",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 15,
                    "exams_name" => "UNBK 2019 Indonesia SMP - C",
                    "total" => "40",
                    "duration" => "120",
                    ])
                ],
                [
                "majors_id" => 2,
                "majors_name" => "IPA",
                "exams" => Array([
                    "exams_id" => 16,
                    "exams_name" => "UNBK 2019 IPA SMP - A",
                    "total" => "40",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 17,
                    "exams_name" => "UNBK 2019 IPA SMP - B",
                    "total" => "40",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 18,
                    "exams_name" => "UNBK 2019 IPA SMP - C",
                    "total" => "40",
                    "duration" => "120",
                    ])
                ],
                [
                "majors_id" => 3,
                "majors_name" => "Matematika",
                "exams" => Array([
                    "exams_id" => 19,
                    "exams_name" => "UNBK 2019 SMP Matematika - A",
                    "total" => "40",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 20,
                    "exams_name" => "UNBK 2019 SMP Matematika - B",
                    "total" => "40",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 21,
                    "exams_name" => "UNBK 2019 SMP Matematika - C",
                    "total" => "40",
                    "duration" => "120",
                    ])
                ],
                [
                "majors_id" => 4,
                "majors_name" => "Bahasa Inggris",
                "exams" => Array([
                    "exams_id" => 22,
                    "exams_name" => "UNBK 2019 Inggris SMP - A",
                    "total" => "40",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 23,
                    "exams_name" => "UNBK 2019 Inggris SMP - B",
                    "total" => "40",
                    "duration" => "90",
                    ],
                    [
                    "exams_id" => 24,
                    "exams_name" => "UNBK 2019 Inggris SMP - C",
                    "total" => "40",
                    "duration" => "120",
                    ])
                ]),
            ],
            [
            "id_grade" => 3,
            "grade" => "SMA",
            "majors" => Array([
                "majors_id" => 1,
                "majors_name" => "Bahasa Indonesia",
                "exams" => Array([
                    "exams_id" => 25,
                    "exams_name" => "UNBK 2019 Indonesia SMA - A",
                    "total" => "50",
                    "duration" => "130",
                    ],
                    [
                    "exams_id" => 26,
                    "exams_name" => "UNBK 2019 Indonesia SMA - B",
                    "total" => "50",
                    "duration" => "130",
                    ],
                    [
                    "exams_id" => 27,
                    "exams_name" => "UNBK 2019 Indonesia SMA - C",
                    "total" => "50",
                    "duration" => "130",
                    ])
                ],
                [
                "majors_id" => 3,
                "majors_name" => "Matematika",
                "exams" => Array([
                    "exams_id" => 28,
                    "exams_name" => "UNBK 2019 Matematika SMA - A",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 29,
                    "exams_name" => "UNBK 2019 Matematika SMA - B",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 30,
                    "exams_name" => "UNBK 2019 Matematika SMA - C",
                    "total" => "50",
                    "duration" => "120",
                    ])
                ],
                [
                "majors_id" => 5,
                "majors_name" => "Fisika",
                "exams" => Array([
                    "exams_id" => 31,
                    "exams_name" => "UNBK 2019 Fisika SMA - A",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 32,
                    "exams_name" => "UNBK 2019 Fisika SMA - B",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 33,
                    "exams_name" => "UNBK 2019 Fisika SMA - C",
                    "total" => "50",
                    "duration" => "120",
                    ])
                ],
                [
                "majors_id" => 6,
                "majors_name" => "Biologi",
                "exams" => Array([
                    "exams_id" => 34,
                    "exams_name" => "UNBK 2019 Biologi SMA - A",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 35,
                    "exams_name" => "UNBK 2019 Biologi SMA - B",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 36,
                    "exams_name" => "UNBK 2019 Biologi SMA - C",
                    "total" => "50",
                    "duration" => "120",
                    ])
                ],
                [
                "majors_id" => 7,
                "majors_name" => "Kimia",
                "exams" => Array([
                    "exams_id" => 37,
                    "exams_name" => "UNBK 2019 Kimia SMA - A",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 38,
                    "exams_name" => "UNBK 2019 Kimia SMA - B",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 39,
                    "exams_name" => "UNBK 2019 Kimia SMA - C",
                    "total" => "50",
                    "duration" => "120",
                    ])
                ],
                [
                "majors_id" => 4,
                "majors_name" => "Bahasa Inggris",
                "exams" => Array([
                    "exams_id" => 40,
                    "exams_name" => "UNBK 2019 Inggris SMA - A",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 41,
                    "exams_name" => "UNBK 2019 Inggris SMA - B",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 42,
                    "exams_name" => "UNBK 2019 Inggris SMA - C",
                    "total" => "50",
                    "duration" => "120",
                    ])
                ],
                [
                "majors_id" => 8,
                "majors_name" => "Geografi",
                "exams" => Array([
                    "exams_id" => 43,
                    "exams_name" => "UNBK 2019 Geografi SMA - A",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 44,
                    "exams_name" => "UNBK 2019 Geografi SMA - B",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 45,
                    "exams_name" => "UNBK 2019 Geografi SMA - C",
                    "total" => "50",
                    "duration" => "120",
                    ])
                ],
                [
                "majors_id" => 9,
                "majors_name" => "Ekonomi",
                "exams" => Array([
                    "exams_id" => 46,
                    "exams_name" => "UNBK 2019 SMA - A",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 47,
                    "exams_name" => "UNBK 2019 SMA - B",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 48,
                    "exams_name" => "UNBK 2019 SMA - C",
                    "total" => "50",
                    "duration" => "120",
                    ])
                ],
                [
                "majors_id" => 10,
                "majors_name" => "Sosiologi",
                "exams" => Array([
                    "exams_id" => 49,
                    "exams_name" => "UNBK 2019 SMA - A",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 50,
                    "exams_name" => "UNBK 2019 SMA - B",
                    "total" => "50",
                    "duration" => "120",
                    ],
                    [
                    "exams_id" => 51,
                    "exams_name" => "UNBK 2019 SMA - C",
                    "total" => "50",
                    "duration" => "120",
                    ])
                ]),
            ],                           
        );
        return $data;
    }
}