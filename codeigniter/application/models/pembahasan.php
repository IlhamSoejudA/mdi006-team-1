<?php 

use GuzzleHttp\Client;
use phpDocumentor\Reflection\Types\Array_;

class Pembahasan extends CI_model {

    public function getPembahasan($examsId){
        $data = Array([
            "id_paket" => 1,
            "total" => "25",
            "pembahasan" => Array([
                "id_pembahasan" => 1,
                "jawaban_benar" => "Aku A1",
                "jawaban_siswa" => "Aku A1",
                "pembahasan" => "Tahukah kamu, A adalah jawaban yang benar.",
                "category" => "C3",
                "question" => "Siapa aku",  
                "ans" => Array([
                    "answer_id" => 2,
                    "data_answers" => Array([
                        "ans" => "Aku A1"
                        ],
                        [
                        "ans" => "Aku B1"
                        ],
                        [
                        "ans" => "Aku C1"
                        ],
                        [
                        "ans" => "Aku D1"
                        ]),
                    ]),
                ],
                [
                "id_pembahasan" => 2,
                "jawaban_benar" => "Aku C2",
                "jawaban_siswa" => "Aku C2",
                "pembahasan" => "Tahukah kamu, C adalah jawaban yang benar.",
                "category" => "C2",
                "question" => "Siapa aku",  
                "ans" => Array([
                    "answer_id" => 2,
                    "data_answers" => Array([
                        "ans" => "Aku A2"
                        ],
                        [
                        "ans" => "Aku B2"
                        ],
                        [
                        "ans" => "Aku C2"
                        ],
                        [
                        "ans" => "Aku D2"
                        ]),
                    ]),
                ],
                    [
                    "id_pembahasan" => 3,
                    "jawaban_benar" => "Aku D3",
                    "jawaban_siswa" => "Aku B3",
                    "pembahasan" => "Tahukah kamu, D adalah jawaban yang benar.",
                    "category" => "C5",
                    "question" => "Siapa mereka",  
                    "ans" => Array([
                        "answer_id" => 3,
                        "data_answers" => Array([
                            "ans" => "Aku A3"
                            ],
                            [
                            "ans" => "Aku B3"
                            ],
                            [
                            "ans" => "Aku C3"
                            ],
                            [
                            "ans" => "Mereka D3"
                            ]),
                        ]),
                    ],
                    [
                    "id_pembahasan" => 4,
                    "jawaban_benar" => "Aku C4",
                    "jawaban_siswa" => "Aku C4",
                    "pembahasan" => "Tahukah kamu, C adalah jawaban yang benar.",
                    "category" => "C2",
                    "question" => "Siapa aku",  
                    "ans" => Array([
                        "answer_id" => 4,
                        "data_answers" => Array([
                            "ans" => "Aku A4"
                            ],
                            [
                            "ans" => "Aku B4"
                            ],
                            [
                            "ans" => "Aku C4"
                            ],
                            [
                            "ans" => "Aku D4"
                            ]),
                        ]),
                    ],
                    [
                    "id_pembahasan" => 5,
                    "jawaban_benar" => "Aku C5",
                    "jawaban_siswa" => "Aku D5",
                    "pembahasan" => "Tahukah kamu, C adalah jawaban yang benar.",
                    "category" => "C2",
                    "question" => "Siapa aku",  
                    "ans" => Array([
                        "answer_id" => 5,
                        "data_answers" => Array([
                            "ans" => "Aku A5"
                            ],
                            [
                            "ans" => "Aku B5"
                            ],
                            [
                            "ans" => "Aku C5"
                            ],
                            [
                            "ans" => "Aku D5"
                            ]),
                        ]),
                    ],
                    [
                    "id_pembahasan" => 6,
                    "jawaban_benar" => "Aku A6",
                    "jawaban_siswa" => "Aku A6",
                    "pembahasan" => "Tahukah kamu, A adalah jawaban yang benar.",
                    "category" => "C2",
                    "question" => "Siapa aku",  
                    "ans" => Array([
                        "answer_id" => 6,
                        "data_answers" => Array([
                            "ans" => "Aku A6"
                            ],
                            [
                            "ans" => "Aku B6"
                            ],
                            [
                            "ans" => "Aku C6"
                            ],
                            [
                            "ans" => "Aku D6"
                            ]),
                        ]),
                    ],
                    [
                    "id_pembahasan" => 7,
                    "jawaban_benar" => "Aku D7",
                    "jawaban_siswa" => "Aku D7",
                    "pembahasan" => "Tahukah kamu, D adalah jawaban yang benar.",
                    "category" => "C2",
                    "question" => "Siapa aku",  
                    "ans" => Array([
                        "answer_id" => 7,
                        "data_answers" => Array([
                            "ans" => "Aku A7"
                            ],
                            [
                            "ans" => "Aku B7"
                            ],
                            [
                            "ans" => "Aku C7"
                            ],
                            [
                            "ans" => "Aku D7"
                            ]),
                        ]),
                    ],
                    [
                    "id_pembahasan" => 8,
                    "jawaban_benar" => "Aku A8",
                    "jawaban_siswa" => "Aku D8",
                    "pembahasan" => "Tahukah kamu, A adalah jawaban yang benar.",
                    "category" => "C2",
                    "question" => "Siapa aku",  
                    "ans" => Array([
                        "answer_id" => 8,
                        "data_answers" => Array([
                            "ans" => "Aku A8"
                            ],
                            [
                            "ans" => "Aku B8"
                            ],
                            [
                            "ans" => "Aku C8"
                            ],
                            [
                            "ans" => "Aku D8"
                            ]),
                        ]),
                    ],
                    [
                    "id_pembahasan" => 9,
                    "jawaban_benar" => "Aku D9",
                    "jawaban_siswa" => "Aku A9",
                    "pembahasan" => "Tahukah kamu, D adalah jawaban yang benar.",
                    "category" => "C2",
                    "question" => "Siapa aku",  
                    "ans" => Array([
                        "answer_id" => 9,
                        "data_answers" => Array([
                            "ans" => "Aku A9"
                            ],
                            [
                            "ans" => "Aku B9"
                            ],
                            [
                            "ans" => "Aku C9"
                            ],
                            [
                            "ans" => "Aku D9"
                            ]),
                        ]),
                    ],
                    [
                    "id_pembahasan" => 10,
                    "jawaban_benar" => "Aku C10",
                    "jawaban_siswa" => "Aku B10",
                    "pembahasan" => "Tahukah kamu, C adalah jawaban yang benar.",
                    "category" => "C2",
                    "question" => "Siapa aku",  
                    "ans" => Array([
                        "answer_id" => 10,
                        "data_answers" => Array([
                            "ans" => "Aku A10"
                            ],
                            [
                            "ans" => "Aku B10"
                            ],
                            [
                            "ans" => "Aku C10"
                            ],
                            [
                            "ans" => "Aku D10"
                            ]),
                        ]),
                    ],
                    [
                    "id_pembahasan" => 11,
                    "jawaban_benar" => "Aku C11",
                    "jawaban_siswa" => "Aku C11",
                    "pembahasan" => "Tahukah kamu, C adalah jawaban yang benar.",
                    "category" => "C2",
                    "question" => "Siapa aku",  
                    "ans" => Array([
                        "answer_id" => 11,
                        "data_answers" => Array([
                            "ans" => "Aku A11"
                            ],
                            [
                            "ans" => "Aku B11"
                            ],
                            [
                            "ans" => "Aku C11"
                            ],
                            [
                            "ans" => "Aku D11"
                            ]),
                        ]),
                    ],
                    [
                    "id_pembahasan" => 12,
                    "jawaban_benar" => "Aku C12",
                    "jawaban_siswa" => "Aku B12",
                    "pembahasan" => "Tahukah kamu, C adalah jawaban yang benar.",
                    "category" => "C2",
                    "question" => "Siapa aku",  
                    "ans" => Array([
                        "answer_id" => 12,
                        "data_answers" => Array([
                            "ans" => "Aku A12"
                            ],
                            [
                            "ans" => "Aku B12"
                            ],
                            [
                            "ans" => "Aku C12"
                            ],
                            [
                            "ans" => "Aku D12"
                            ]),
                        ]),
                    ],
                    [
                    "id_pembahasan" => 13,
                    "jawaban_benar" => "Aku C13",
                    "jawaban_siswa" => "Aku C13",
                    "pembahasan" => "Tahukah kamu, C adalah jawaban yang benar.",
                    "category" => "C2",
                    "question" => "Siapa aku",  
                    "ans" => Array([
                        "answer_id" => 13,
                        "data_answers" => Array([
                            "ans" => "Aku A13"
                            ],
                            [
                            "ans" => "Aku B13"
                            ],
                            [
                            "ans" => "Aku C13"
                            ],
                            [
                            "ans" => "Aku D13"
                            ]),
                        ]),
                    ],
                    [
                    "id_pembahasan" => 14,
                    "jawaban_benar" => "Aku A14",
                    "jawaban_siswa" => "Aku C14",
                    "pembahasan" => "Tahukah kamu, A adalah jawaban yang benar.",
                    "category" => "C2",
                    "question" => "Siapa aku",  
                    "ans" => Array([
                        "answer_id" => 14,
                        "data_answers" => Array([
                            "ans" => "Aku A14"
                            ],
                            [
                            "ans" => "Aku B14"
                            ],
                            [
                            "ans" => "Aku C14"
                            ],
                            [
                            "ans" => "Aku D14"
                            ]),
                        ]),
                    ],
                    [
                    "id_pembahasan" => 15,
                    "jawaban_benar" => "Aku A15",
                    "jawaban_siswa" => "Aku B15",
                    "pembahasan" => "Tahukah kamu, A adalah jawaban yang benar.",
                    "category" => "C2",
                    "question" => "Siapa aku",  
                    "ans" => Array([
                        "answer_id" => 15,
                        "data_answers" => Array([
                            "ans" => "Aku A15"
                            ],
                            [
                            "ans" => "Aku B15"
                            ],
                            [
                            "ans" => "Aku C15"
                            ],
                            [
                            "ans" => "Aku D15"
                            ]),
                        ]),
                    ],
                    [
                    "id_pembahasan" => 16,
                    "jawaban_benar" => "Aku B16",
                    "jawaban_siswa" => "Aku D16",
                    "pembahasan" => "Tahukah kamu, B adalah jawaban yang benar.",
                    "category" => "C2",
                    "question" => "Siapa aku",  
                    "ans" => Array([
                        "answer_id" => 16,
                        "data_answers" => Array([
                            "ans" => "Aku A16"
                            ],
                            [
                            "ans" => "Aku B16"
                            ],
                            [
                            "ans" => "Aku C16"
                            ],
                            [
                            "ans" => "Aku D16"
                            ]),
                        ]),
                    ],
                    [
                    "id_pembahasan" => 17,
                    "jawaban_benar" => "Aku D17",
                    "jawaban_siswa" => "Aku B17",
                    "pembahasan" => "Tahukah kamu, D adalah jawaban yang benar.",
                    "category" => "C2",
                    "question" => "Siapa aku",  
                    "ans" => Array([
                        "answer_id" => 17,
                        "data_answers" => Array([
                            "ans" => "Aku A17"
                            ],
                            [
                            "ans" => "Aku B17"
                            ],
                            [
                            "ans" => "Aku C17"
                            ],
                            [
                            "ans" => "Aku D17"
                            ]),
                        ]),
                    ],
                    [
                    "id_pembahasan" => 18,
                    "jawaban_benar" => "Aku B18",
                    "jawaban_siswa" => "Aku A18",
                    "pembahasan" => "Tahukah kamu, B adalah jawaban yang benar.",
                    "category" => "C2",
                    "question" => "Siapa aku",  
                    "ans" => Array([
                        "answer_id" => 18,
                        "data_answers" => Array([
                            "ans" => "Aku A18"
                            ],
                            [
                            "ans" => "Aku B18"
                            ],
                            [
                            "ans" => "Aku C18"
                            ],
                            [
                            "ans" => "Aku D18"
                            ]),
                        ]),
                    ],
                    [
                    "id_pembahasan" => 19,
                    "jawaban_benar" => "Aku C19",
                    "jawaban_siswa" => "Aku C19",
                    "pembahasan" => "Tahukah kamu, C adalah jawaban yang benar.",
                    "category" => "C2",
                    "question" => "Siapa aku",  
                    "ans" => Array([
                        "answer_id" => 19,
                        "data_answers" => Array([
                            "ans" => "Aku A19"
                            ],
                            [
                            "ans" => "Aku B19"
                            ],
                            [
                            "ans" => "Aku C19"
                            ],
                            [
                            "ans" => "Aku D19"
                            ]),
                        ]),
                    ],
                    [
                    "id_pembahasan" => 20,
                    "jawaban_benar" => "Aku D20",
                    "jawaban_siswa" => "Aku A20",
                    "pembahasan" => "Tahukah kamu, D adalah jawaban yang benar.",
                    "category" => "C4",
                    "question" => "Siapa aku",  
                    "ans" => Array([
                        "answer_id" => 20,
                        "data_answers" => Array([
                            "ans" => "Aku A20"
                            ],
                            [
                            "ans" => "Aku B20"
                            ],
                            [
                            "ans" => "Aku C20"
                            ],
                            [
                            "ans" => "Aku D20"
                            ]),
                        ]),
                    ],
                    [
                    "id_pembahasan" => 21,
                    "jawaban_benar" => "Aku C21",
                    "jawaban_siswa" => "Aku C21",
                    "pembahasan" => "Tahukah kamu, C adalah jawaban yang benar.",
                    "category" => "C4",
                    "question" => "Siapa aku",  
                    "ans" => Array([
                        "answer_id" => 21,
                        "data_answers" => Array([
                            "ans" => "Aku A21"
                            ],
                            [
                            "ans" => "Aku B21"
                            ],
                            [
                            "ans" => "Aku C21"
                            ],
                            [
                            "ans" => "Aku D21"
                            ]),
                        ]),
                    ],
                    [
                    "id_pembahasan" => 22,
                    "jawaban_benar" => "Aku D22",
                    "jawaban_siswa" => "Aku A22",
                    "pembahasan" => "Tahukah kamu, D adalah jawaban yang benar.",
                    "category" => "C3",
                    "question" => "Siapa aku",  
                    "ans" => Array([
                        "answer_id" => 22,
                        "data_answers" => Array([
                            "ans" => "Aku A22"
                            ],
                            [
                            "ans" => "Aku B22"
                            ],
                            [
                            "ans" => "Aku C22"
                            ],
                            [
                            "ans" => "Aku D22"
                            ]),
                        ]),
                    ],
                    [
                    "id_pembahasan" => 23,
                    "jawaban_benar" => "Aku A23",
                    "jawaban_siswa" => "Aku A23",
                    "pembahasan" => "Tahukah kamu, A adalah jawaban yang benar.",
                    "category" => "C5",
                    "question" => "Siapa aku",  
                    "ans" => Array([
                        "answer_id" => 23,
                        "data_answers" => Array([
                            "ans" => "Aku A23"
                            ],
                            [
                            "ans" => "Aku B23"
                            ],
                            [
                            "ans" => "Aku C23"
                            ],
                            [
                            "ans" => "Aku D23"
                            ]),
                        ]),
                    ],
                    [
                    "id_pembahasan" => 24,
                    "jawaban_benar" => "Aku A24",
                    "jawaban_siswa" => "Aku A24",
                    "pembahasan" => "Tahukah kamu, A adalah jawaban yang benar.",
                    "category" => "C6",
                    "question" => "Siapa aku",  
                    "ans" => Array([
                        "answer_id" => 24,
                        "data_answers" => Array([
                            "ans" => "Aku A24"
                            ],
                            [
                            "ans" => "Aku B24"
                            ],
                            [
                            "ans" => "Aku C24"
                            ],
                            [
                            "ans" => "Aku D24"
                            ]),
                        ]),
                    ],
                    [
                    "id_pembahasan" => 25,
                    "jawaban_benar" => "Aku B25",
                    "jawaban_siswa" => "Aku A25",
                    "pembahasan" => "Tahukah kamu, B adalah jawaban yang benar.",
                    "category" => "C1",
                    "question" => "Siapa aku",  
                    "ans" => Array([
                        "answer_id" => 25,
                        "data_answers" => Array([
                            "ans" => "Aku A25"
                            ],
                            [
                            "ans" => "Aku B25"
                            ],
                            [
                            "ans" => "Aku C25"
                            ],
                            [
                            "ans" => "Aku D25"
                            ]),
                        ]),
                    ]),
                ],
    );
        return $data;
    }
} 