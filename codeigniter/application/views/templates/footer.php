<div class="footer-wp">
<div class="container">
        <div class="row">
        <div class="col-md-4">
                <div class="text-lp">
                <img src="<?php echo base_url("assets/logo-mejakita.svg") ?>" class="logo-mejakita"/>
                <div class="footer-text">Jadilah bagian dari pertumbuhan kami. Bergabunglah dengan Mejakita dan pastikan dirimu menjadi bagian dari sebuah perubahan untuk pendidikan Indonesia yang lebih baik!</div>
                </div>
        </div>
        <div class="col-md-8">
        <div class="footer-ltext">
        <div class="row">
                <div class="col-md-3">
                        <h6>Tentang</h6>
                        <a href="#">Mejakita</a><br/>
                        <a href="#">Tim Kami</a>
                </div>
                <div class="col-md-3">
                        <h6>Materi</h6>
                        <a href="#">Matematika</a><br/>
                        <a href="#">Fisika</a><br/>
                        <a href="#">Biologi</a><br/>
                        <a href="#">Kimia</a><br/>
                        <a href="#">Sosiologi</a><br/>
                </div>
                <div class="col-md-3">
                        <h6>Support</h6>
                        <a href="#">Hubungi Kami</a><br/>
                        <a href="#">Kontributor</a><br/>
                        <a href="#">F.A.Q</a><br/>
                </div>
                <div class="col-md-3">
                        <h6>Tentang</h6>
                        <div class="footer-i">
                        <ul>
                                <li>
                                <a href="#"><img src="<?php echo base_url("assets/i-facebook.svg") ?>" class="i-facebook"/></a><br/>
                                </li>
                                <li>
                                <a href="#"><img src="<?php echo base_url("assets/i-twitter.svg") ?>" class="i-twitter"/></a><br/>
                                </li>
                                <li>
                                <a href="#"><img src="<?php echo base_url("assets/i-instagram.svg") ?>" class="i-instagram"/></a><br/>
                                </li>
                                <li>
                                <a href="#"><img src="<?php echo base_url("assets/i-youtube.svg") ?>" class="i-youtube"/></a><br/>
                                </li>
                        </ul>
                        </div>
                </div>
        </div>
        </div>
        </div>
</div>