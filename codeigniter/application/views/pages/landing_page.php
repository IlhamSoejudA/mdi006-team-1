<head>
	<title><?php echo $title ?></title>
</head>
<div class="wrapper-lp">
	<div class="container">
		<div class="row">
			<div class="col-md-7 col-sm-7">
				<div class="text-lp">
					<h1>TRY OUT</h1>
					<br/>
					<p>Latihan soal tryout ujian nasional tingkat SD,SMP dan SMK dapat dikerjakan di mejakita. Mejakita memberikan fasilitas latihan online gratis untuk mata pelajaran yang di ujikan. Kamu dapat belajar dan mengulangi ujian apabila nilai kurang.</p>
					<a href="<?php echo base_url('event') ?>">
						<div class="button-lp">
							<div class="button-text">
								Mulai
							</div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-md-5 col-sm-5">
				<img src="assets/2.svg" class="img-lp r"/>
			</div>
			</div>
		</div>
	</div>
	<div class="wrapper-lp2">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="alur-text">
					<h2>Alur Tryout</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-4">
				<div class="frame-mapel">
					<center>
					<img src="assets/no1.svg" class="nomor-fr"/><br/>
					<img src="assets/icon1.svg" class="icon-fr"/><br/>
					<div class="frame-mapel-text">
						<h5>Pilih Mapel</h5><br/>
						<p>Pilih Mata Pelajaran yang ingin kamu pelajari</p><br/>
					</div>
					</center>
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="frame-mapel">
					<center>
					<img src="assets/no2.svg" class="nomor-fr"/><br/>
					<img src="assets/icon2.svg" class="icon-fr"/><br/>
					<div class="frame-mapel-text">
						<h5>Petunjuk</h5><br/>
						<p>Baca petunjuk pengerjaan soal dengan cermat</p><br/>
					</div>
					</center>
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="frame-mapel">
					<center>
					<img src="assets/no3.svg" class="nomor-fr"/><br/>
					<img src="assets/icon3.svg" class="icon-fr"/><br/>
					<div class="frame-mapel-text">
						<h5>Kerjakan</h5><br/>
						<p>Terdapat jumlah soal yang berbeda pada tiap Mata Pelajaran sesuai dengan jenjang pendidikan</p><br/>
					</div>
					</center>
				</div>
			</div>
		</div>
		<div class="row">
		<div class="col-md-4 col-sm-4">
				<div class="frame-mapel">
					<center>
					<img src="assets/no4.svg" class="nomor-fr"/><br/>
					<img src="assets/icon4.svg" class="icon-fr"/><br/>
					<div class="frame-mapel-text">
						<h5>Selesaikan</h5><br/>
						<p>Pastikan semua soal telah terjawab dan klik tombol kirim jawaban</p><br/>
					</div>
					</center>
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="frame-mapel">
					<center>
					<img src="assets/no5.svg" class="nomor-fr"/><br/>
					<img src="assets/icon5.svg" class="icon-fr"/><br/>
					<div class="frame-mapel-text">
						<h5>Hasil dan Pembahasan</h5><br/>
						<p>Nilai dan pembahasan pada setiap materi dapat dilihat dengan detail</p><br/>
					</div>
					</center>
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="frame-mapel">
					<center>
					<img src="assets/no6.svg" class="nomor-fr"/><br/>
					<img src="assets/icon6.svg" class="icon-fr"/><br/>
					<div class="frame-mapel-text">
						<h5>Analisis</h5><br/>
						<p>Lihat analisis kemampuanmu berdasarkan soal yang telah dikerjakan</p><br/>
					</div>
					</center>
				</div>
			</div>
		</div>
	</div>
	</div>
	
</body>
</html>