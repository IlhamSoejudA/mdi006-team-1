<head>
	<title><?php echo $title ?></title>
</head>
<section>
<div class="wrapper-ps">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-7 col-sm-7">
				<div class="text-ps">
					<h1>MATA PELAJARAN UJIAN NASIONAL</h1>
					<br/>
					<p>Pilih Mata Pelajaran Ujian Nasional dari tiap jenjang pendidikan. Tersedia beberapa paket soal Ujian Nasional dari tahun ke tahun.</p>
				</div>
			</div>
			<div class="col-md-5 col-sm-5">
				<img src="assets/paket-soal.svg" class="img-ps"/>
			</div>
			</div>
		</div>
	</section>
	<section>
	<div class="wrapper-sd">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="ps-text">
				<center>
					<h2>Ujian Nasional Tingkat <?= $sd['grade'];?></h2>
				</center>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<center>
				<div class="img-ps2">
						<img src="assets/sd.svg"/>
				</div>
				</center>
			</div>
		</div>
		<div class="row">
		<div class="col-md-3 col-sm-3">
				<div class="frame-ps">
					<center>
					<img src="assets/indonesia.svg" class="icon-ps"/><br/>
					<div class="frame-mapel-text-ps">
						<h5><?= $mapel1['major'];?></h5><br/>
						<p>Terdiri dari 7 Variasi Soal Ujian Setiap Tahun</p><br/>
					</div>
					<a href="<?php echo base_url('list_ujian_sd') ?>">
						<div class="button-ps">
							<div class="button-text-ps">
								<center>
									Lihat
								</center>
							</div>
						</div>
					</a>
					</center>
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<div class="frame-ps">
					<center>
					<img src="assets/ipa.svg" class="icon-ps"/><br/>
					<div class="frame-mapel-text-ps">
						<h5><?= $mapel2['major'];?></h5><br/>
						<p>Terdiri dari 7 Variasi Soal Ujian Setiap Tahun</p><br/>
					</div>
					<a href="<?php echo base_url('list_ujian_sd') ?>">
						<div class="button-ps">
							<div class="button-text-ps">
								Lihat
							</div>
						</div>
					</a>
					</center>
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<div class="frame-ps">
					<center>
					<img src="assets/matematika.svg" class="icon-ps"/><br/>
					<div class="frame-mapel-text-ps">
						<h5><?= $mapel3['major'];?></h5><br/>
						<p>Terdiri dari 7 Variasi Soal Ujian Setiap Tahun</p><br/>
					</div>
					<a href="<?php echo base_url('list_ujian_sd') ?>">
						<div class="button-ps">
							<div class="button-text-ps">
								Lihat
							</div>
						</div>
					</a>
					</center>
				</div>
			</div>
		</div>
	</div>
	</div>
	<div class="wrapper-smp">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="ps-text">
				<center>
					<h2>Ujian Nasional Tingkat <?= $smp['grade'];?></h2>
				</center>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<center>
				<div class="img-ps2">
						<img src="assets/smp.svg"/>
				</div>
				</center>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 col-sm-3">
				<div class="frame-ps">
					<center>
					<img src="assets/indonesia.svg" class="icon-ps"/><br/>
					<div class="frame-mapel-text-ps">
						<h5><?= $mapel1['major'];?></h5><br/>
						<p>Terdiri dari 7 Variasi Soal Ujian Setiap Tahun</p><br/>
					</div>
					<a href="<?php echo base_url('list_ujian_smp') ?>">
						<div class="button-ps">
							<div class="button-text-ps">
								Lihat
							</div>
						</div>
					</a>
					</center>
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<div class="frame-ps">
					<center>
					<img src="assets/ipa.svg" class="icon-ps"/><br/>
					<div class="frame-mapel-text-ps">
						<h5><?= $mapel2['major'];?></h5><br/>
						<p>Terdiri dari 7 Variasi Soal Ujian Setiap Tahun</p><br/>
					</div>
					<a href="<?php echo base_url('list_ujian_smp') ?>">
						<div class="button-ps">
							<div class="button-text-ps">
								Lihat
							</div>
						</div>
					</a>
					</center>
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<div class="frame-ps">
					<center>
					<img src="assets/matematika.svg" class="icon-ps"/><br/>
					<div class="frame-mapel-text-ps">
						<h5><?= $mapel3['major'];?></h5><br/>
						<p>Terdiri dari 7 Variasi Soal Ujian Setiap Tahun</p><br/>
					</div>
					<a href="<?php echo base_url('list_ujian_smp') ?>">
						<div class="button-ps">
							<div class="button-text-ps">
								Lihat
							</div>
						</div>
					</a>
					</center>
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<div class="frame-ps">
					<center>
					<img src="assets/bahasa inggris.svg" class="icon-ps"/><br/>
					<div class="frame-mapel-text-ps">
						<h5><?= $mapel4['major'];?></h5><br/>
						<p>Terdiri dari 7 Variasi Soal Ujian Setiap Tahun</p><br/>
					</div>
					<a href="<?php echo base_url('list_ujian_smp') ?>">
						<div class="button-ps">
							<div class="button-text-ps">
								Lihat
							</div>
						</div>
					</a>
					</center>
				</div>
			</div>
		</div>
	</div>
	</div>
	<div class="wrapper-sma">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="ps-text">
				<center>
					<h2>Ujian Nasional Tingkat <?= $sma['grade'];?></h2>
				</center>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<center>
				<div class="img-ps2">
						<img src="assets/sma.svg"/>
				</div>
				</center>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 col-sm-3">
				<div class="frame-ps">
					<center>
					<img src="assets/indonesia.svg" class="icon-ps"/><br/>
					<div class="frame-mapel-text-ps">
						<h5><?= $mapel1['major'];?></h5><br/>
						<p>Terdiri dari 7 Variasi Soal Ujian Setiap Tahun</p><br/>
					</div>
					<a href="<?php echo base_url('list_ujian_sma') ?>">
						<div class="button-ps">
							<div class="button-text-ps">
								Lihat
							</div>
						</div>
					</a>
					</center>
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<div class="frame-ps">
					<center>
					<img src="assets/matematika.svg" class="icon-ps"/><br/>
					<div class="frame-mapel-text-ps">
						<h5><?= $mapel3['major'];?></h5><br/>
						<p>Terdiri dari 7 Variasi Soal Ujian Setiap Tahun</p><br/>
					</div>
					<a href="<?php echo base_url('list_ujian_sma') ?>">
						<div class="button-ps">
							<div class="button-text-ps">
								Lihat
							</div>
						</div>
					</a>
					</center>
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<div class="frame-ps">
					<center>
					<img src="assets/fisika.svg" class="icon-ps"/><br/>
					<div class="frame-mapel-text-ps">
						<h5><?= $mapel5['major'];?></h5><br/>
						<p>Terdiri dari 7 Variasi Soal Ujian Setiap Tahun</p><br/>
					</div>
					<a href="<?php echo base_url('list_ujian_sma') ?>">
						<div class="button-ps">
							<div class="button-text-ps">
								Lihat
							</div>
						</div>
					</a>
					</center>
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<div class="frame-ps">
					<center>
					<img src="assets/biologi.svg" class="icon-ps"/><br/>
					<div class="frame-mapel-text-ps">
						<h5>Biologi</h5><br/>
						<p>Terdiri dari 7 Variasi Soal Ujian Setiap Tahun</p><br/>
					</div>
					<a href="<?php echo base_url('list_ujian_sma') ?>">
						<div class="button-ps">
							<div class="button-text-ps">
								Lihat
							</div>
						</div>
					</a>
					</center>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 col-sm-3">
				<div class="frame-ps">
					<center>
					<img src="assets/kimia.svg" class="icon-ps"/><br/>
					<div class="frame-mapel-text-ps">
						<h5>Kimia</h5><br/>
						<p>Terdiri dari 7 Variasi Soal Ujian Setiap Tahun</p><br/>
					</div>
					<a href="<?php echo base_url('list_ujian_sma') ?>">
						<div class="button-ps">
							<div class="button-text-ps">
								Lihat
							</div>
						</div>
					</a>
					</center>
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<div class="frame-ps">
					<center>
					<img src="assets/bahasa inggris.svg" class="icon-ps"/><br/>
					<div class="frame-mapel-text-ps">
						<h5><?= $mapel4['major'];?></h5><br/>
						<p>Terdiri dari 7 Variasi Soal Ujian Setiap Tahun</p><br/>
					</div>
					<a href="<?php echo base_url('list_ujian_sma') ?>">
						<div class="button-ps">
							<div class="button-text-ps">
								Lihat
							</div>
						</div>
					</a>
					</center>
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<div class="frame-ps">
					<center>
					<img src="assets/geografi.svg" class="icon-ps"/><br/>
					<div class="frame-mapel-text-ps">
						<h5>Geografi</h5><br/>
						<p>Terdiri dari 7 Variasi Soal Ujian Setiap Tahun</p><br/>
					</div>
					<a href="<?php echo base_url('list_ujian_sma') ?>">
						<div class="button-ps">
							<div class="button-text-ps">
								Lihat
							</div>
						</div>
					</a>
					</center>
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<div class="frame-ps">
					<center>
					<img src="assets/ekonomi.svg" class="icon-ps"/><br/>
					<div class="frame-mapel-text-ps">
						<h5>Ekonomi</h5><br/>
						<p>Terdiri dari 7 Variasi Soal Ujian Setiap Tahun</p><br/>
					</div>
					<a href="<?php echo base_url('list_ujian_sma') ?>">
						<div class="button-ps">
							<div class="button-text-ps">
								Lihat
							</div>
						</div>
					</a>
					</center>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 col-sm-3">
				<div class="frame-ps">
					<center>
					<img src="assets/sosiologi.svg" class="icon-ps"/><br/>
					<div class="frame-mapel-text-ps">
						<h5>Sosiologi</h5><br/>
						<p>Terdiri dari 7 Variasi Soal Ujian Setiap Tahun</p><br/>
					</div>
					<a href="<?php echo base_url('list_ujian_sma') ?>">
						<div class="button-ps">
							<div class="button-text-ps">
								Lihat
							</div>
						</div>
					</a>
					</center>
				</div>
			</div>
		</div>
	</div>
	</div>
	
	</section>
	
</body>
</html>