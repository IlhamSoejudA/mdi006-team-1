<head>
	<title><?php echo $title ?></title>
</head>
	<body>
<section>
	<div class="wrapper-ps">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-7 col-sm-7">
					<div class="text-ps">
						<h1>MATA PELAJARAN UJIAN NASIONAL</h1>
						<br/>
						<p>Pilih Mata Pelajaran Ujian Nasional dari tiap jenjang pendidikan. Tersedia beberapa paket soal Ujian Nasional dari tahun ke tahun.</p>
					</div>
				</div>
				<div class="col-md-5 col-sm-5">
					<img src="assets/paket-soal.svg" class="img-ps"/>
				</div>
			</div>
		</div>
	</div>
</section>
	<?php foreach ($data as $event){?>
<section>
<?php 
	if($event["grade"] == 'SD'){
		$wrp = 'wrapper-sd'; 
	}
	else if($event["grade"] == 'SMP'){
		$wrp = 'wrapper-smp'; 
	}
	else if($event["grade"] == 'SMA'){
		$wrp = 'wrapper-sma'; 
	}
?>
	<div class="<?php echo $wrp ?>">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="ps-text">
				<center>
					<h2>Ujian Nasional Tingkat <?php echo $event["grade"];?></h2>
				</center>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<center>
				<div class="img-ps2">
				<?php 
					if($event["grade"] == 'SD'){
						$gbr = 'assets/sd.svg'; 
					}
					else if($event["grade"] == 'SMP'){
						$gbr = 'assets/smp.svg'; 
					}
					else if($event["grade"] == 'SMA'){
						$gbr = 'assets/sma.svg'; 
					}
					?>
						<img src="<?php echo $gbr ?>"/>
				</div>
				</center>
			</div>
		</div>
		<div class="row">
			<?php 
				foreach ($event["majors"] as $in){
			?>
			<div class="col-md-3 col-sm-3">
				<div class="frame-ps">
					<center>
					<img src="assets/indonesia.svg" class="icon-ps"/><br/>
					<div class="frame-mapel-text-ps">
						<h5><?php echo $in['majors_name'];?></h5><br/>
						<?php
						$majors_name = $in['majors_name'];
						if($in['majors_name'] == $majors_name){
							$total=0;
							foreach ($in["exams"] as $list_ujian){
							$total++;
						?>
						<?php }} ?>
						<p>Terdiri dari <?php echo $total ?> Variasi Soal Ujian Setiap Tahun</p><br/>
						
					</div>
					<a href="<?php echo base_url("event/detail/".$event["id_grade"]."/".$in['majors_id']) ?>">
						<div class="button-ps">
							<div class="button-text-ps">
								<center>
									Lihat
								</center>
							</div>
						</div>
					</a>
					</center>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
	</div>
	<?php } ?>
</section>


	</body>
</html>
