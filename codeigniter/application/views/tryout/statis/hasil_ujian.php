<head>
	<title><?php echo $title ?></title>
</head>
<section>
	<div class="wrapper-hu">
			<div class="bg-hu">
				<img src="assets/banner.jpg" width="100%" height="366px">
			</div>									
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<center>
						<div class="text-hu">
							<h3>Hai "data['nama']" . . . </h3>
							<p>Terimakasih sudah menyelesaikan Try Out dengan sungguh-sungguh</p>
						</div>
					</center>
				</div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="wrapper-hu2">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="text-hu2">
				<center>
					Hasil Latihan Soal
				</center>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="table-hu">
					<table align="center">
						<tr>
						<td class="td-data">Mata Pelajaran</td>
						<td class="td-data">[Data Mata Pelajaran]</td>
						</tr>
						<tr>
						<td class="td-data">Paket Soal</td>
						<td class="td-data">[Data Paket Soal]</td>
						</tr>
						<tr>
						<td class="td-data">Waktu Pengerjaan</td>
						<td class="td-data">[Data Waktu Pengerjaan]</td>
						</tr>
						<tr>
						<td class="td-data">Jawaban Benar</td>
						<td class="td-data">[Data Jawaban Benar]</td>
						</tr>
						<tr>
						<td class="td-data">Jawaban Salah</td>
						<td class="td-data">[Data Jawaban Salah]</td>
						</tr>
						<tr>
						<th class="td-data">Nilai</th>
						<th class="td-data">[Data Nilai]</th>
						</tr>
						<tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="btn-hasil r">
					<center>
						<a href="<?php echo base_url('hasil_pembahasan') ?>">
							<div class="btn-pa"> Lihat Pembahasan</div>
						</a>
					</center>
				</div>
			</div>
			<div class="col-md-6">
				<div class="btn-hasil l">
					<center>
						<a href="<?php echo base_url('hasil_analisis') ?>">
							<div class="btn-pa"> Lihat Analisis</div>
						</a>
					</center>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>
	
</body>
</html>