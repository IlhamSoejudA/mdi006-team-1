<head>
	<title><?php echo $title ?></title>
</head>
<section>
	<div class="wrapper-hu">
		<div class="bg-hu">
			<img src="assets/banner.jpg" width="100%" height="366px">
		</div>									
		<div class="container">
			<div class="row">
			<div class="col-md-12">
				<div class="hero-text">
					<div class="text-hu">
					<center>
						<h3>"data['nama']",</h3>
						<p>Kamu adalah siswa yang memiliki ingatan kuat tapi kamu perlu meningkatkan kemampuan analisismu</p>
					</center>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="wrapper-hu2">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="text-hu2">
				<center>
					Analisis Kemampuan
				</center>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="table-hu">
					<table align="center">
						<tr>
						<td class="td-data">Kemampuan menghafal/mengingat (C1)</td>
						<td class="td-data">[Data C1]</td>
						</tr>
						<tr>
						<td class="td-data">Kemampuan memahami (C2)</td>
						<td class="td-data">[Data C2]</td>
						</tr>
						<tr>
						<td class="td-data">Kemampuan menerapkan (C3)</td>
						<td class="td-data">[Data C3]</td>
						</tr>
						<tr>
						<td class="td-data">Kemampuan menganalisis (C4)</td>
						<td class="td-data">[Data C4]</td>
						</tr>
						<tr>
						<td class="td-data">Kemampuan sintesis (C5)</td>
						<td class="td-data">[Data C5]</td>
						</tr>
						<tr>
						<th class="td-data">Kemampuan mengevaluasi (C6)</th>
						<th class="td-data">[Data C6]</th>
						</tr>
						<tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="btn-hasil r">
					<center>
						<a href="<?php echo base_url('hasil_ujian') ?>">
							<div class="btn-pa">Kembali</div>
						</a>
					</center>
				</div>
			</div>
			<div class="col-md-6">
				<div class="btn-hasil l">
					<center>
						<a href="<?php echo base_url('hasil_pembahasan') ?>">
							<div class="btn-pa">Lihat Pembahasan</div>
						</a>
					</center>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>
	
</body>
</html>