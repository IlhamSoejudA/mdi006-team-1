<head>
	<title><?php echo $title ?></title>
</head>
<body>
<section>
	<div class="wrapper-mu">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-7">
				<div class="mu-text">
					<div class="table-mu">
						<table>
							<tr>
							<td>Tingkat</td>
							<td class="table-mu-td">: SMP</td>
							</tr>
							<tr>
							<td>Mata Pelajaran</td>
							<td class="table-mu-td">: Bahasa Inggris</td>
							</tr>
							<tr>
							<td>Paket Soal</td>
							<td class="table-mu-td">: UN 2015 - A</td>
							</tr>
						</table>
					</div>
				</div>
				</div>
				<div class="col-md-2 r">
					<center>
					<div class="mu-clock r">
						<img src="assets/i-clock.svg"/>  
						<span id="clockdiv"><b></b>
							<span class="hours"></span>
							<span class="minutes"></span>
							<span class="seconds"></span></b>
						</span>
					</div>
					</center>
				</div>
				<div class="col-md-3">
					<div class="mu-text r">
						<div class="table-mu">
							<table>
								<tr>
								<th colspan="2">Keterangan Nomor Soal</th>
								</tr>
								<tr>
								<td><img src="assets/i-green.svg"/></td>
								<td class="table-mu-td"> Sudah dijawab</td>
								</tr>
								<tr>
								<td><img src="assets/i-black.svg"/></td>
								<td class="table-mu-td"> Belum dijawab</td>
								</tr>
								<tr>
								<td><img src="assets/i-yellow.svg"/></td>
								<td class="table-mu-td"> Tandai</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
	</div>
	</div>
</section>
<section>
	<div class="wrapper-mu2">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8">
				<div class="soal">
					<div id="main-slider" class="flexslider">
						<ul class="slides">
							<li>
							<div class="btn-tandai text-b">1</div>
							<p>1) What is Lorem Ipsum? Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
							Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
							when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
							It has survived not only five centuries, but also the leap into electronic typesetting, 
							remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset 
							sheets containing Lorem Ipsum passages, and more recently with desktop 
							publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
							</li>
							<li>
							<div class="btn-tandai text-b">2</div>
							<p>2) Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
							when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
							It has survived not only five centuries, but also the leap into electronic typesetting, 
							remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset 
							sheets containing Lorem Ipsum passages, and more recently with desktop 
							publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
							</li>
							<li>
							<div class="btn-tandai text-b">3</div>
							<p>3) It was popularised in the 1960s with the release of Letraset 
							sheets containing Lorem Ipsum passages, and more recently with desktop 
							publishing software like Aldus PageMaker including versions of Lorem Ipsum. 
							Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
							when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
							It has survived not only five centuries, but also the leap into electronic typesetting, 
							remaining essentially unchanged.</p>
							</li>
							<li>
							<div class="btn-tandai text-b">4</div>
							<p>3) It was popularised in the 1960s with the release of Letraset 
							sheets containing Lorem Ipsum passages, and more recently with desktop 
							publishing software like Aldus PageMaker including versions of Lorem Ipsum. 
							Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
							when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
							It has survived not only five centuries, but also the leap into electronic typesetting, 
							remaining essentially unchanged.</p>
							</li>
							<li>
							<div class="btn-tandai text-b">5</div>
							<p>3) It was popularised in the 1960s with the release of Letraset 
							sheets containing Lorem Ipsum passages, and more recently with desktop 
							publishing software like Aldus PageMaker including versions of Lorem Ipsum. 
							Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
							when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
							It has survived not only five centuries, but also the leap into electronic typesetting, 
							remaining essentially unchanged.</p>
							</li>
							<li>
							<div class="btn-tandai text-b">6</div>
							<p>3) It was popularised in the 1960s with the release of Letraset 
							sheets containing Lorem Ipsum passages, and more recently with desktop 
							publishing software like Aldus PageMaker including versions of Lorem Ipsum. 
							Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
							when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
							It has survived not only five centuries, but also the leap into electronic typesetting, 
							remaining essentially unchanged.</p>
							</li>
							<li>
							<div class="btn-tandai text-b">7</div>
							<p>3) It was popularised in the 1960s with the release of Letraset 
							sheets containing Lorem Ipsum passages, and more recently with desktop 
							publishing software like Aldus PageMaker including versions of Lorem Ipsum. 
							Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
							when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
							It has survived not only five centuries, but also the leap into electronic typesetting, 
							remaining essentially unchanged.</p>
							</li>
							<li>
							<div class="btn-tandai text-b">8</div>
							<p>3) It was popularised in the 1960s with the release of Letraset 
							sheets containing Lorem Ipsum passages, and more recently with desktop 
							publishing software like Aldus PageMaker including versions of Lorem Ipsum. 
							Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
							when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
							It has survived not only five centuries, but also the leap into electronic typesetting, 
							remaining essentially unchanged.</p>
							</li>
							<li>
							<div class="btn-tandai text-b">9</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">10</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">11</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">12</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">13</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">14</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">15</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">16</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">17</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">18</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">19</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">20</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">21</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">22</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">23</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">24</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">25</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">26</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">27</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">28</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">29</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">30</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">31</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">32</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">33</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">34</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">35</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">36</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">37</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">38</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">39</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">40</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">41</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">42</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">43</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">44</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">45</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">46</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">47</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">48</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">49</div>
							<p>3) </p>
							</li>
							<li>
							<div class="btn-tandai text-b">50</div>
							<p>3) </p>
							</li>
						</ul>
					</div>
				</div>
				<form action="<?php echo base_url('hasil_ujian') ?>" method="post" name="latihan_soal" id="latihan_soal">
				<div class="jawaban">
					<div class="flexslider_children">
						<ul class="slides">
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction()" name="j1" value="a"/>Data Jawaban a: 1</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction()" name="j1" value="b"/>Data Jawaban b: 1</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction()" name="j1" value="c"/>Data Jawaban c: 1</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction()" name="j1" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction()" name="j1" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction2()" name="j2" value="a"/>Data Jawaban a: 2</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction2()" name="j2" value="b"/>Data Jawaban b: 2</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction2()" name="j2" value="c"/>Data Jawaban c: 2</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction2()" name="j2" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction2()" name="j2" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction3()" name="j3" value="a"/>Data Jawaban a: 3</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction3()" name="j3" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction3()" name="j3" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction3()" name="j3" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction3()" name="j3" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction4()" name="j4" value="a"/>Data Jawaban a: 4</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction4()" name="j4" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction4()" name="j4" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction4()" name="j4" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction4()" name="j4" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction5()" name="j5" value="a"/>Data Jawaban a: 5</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction5()" name="j5" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction5()" name="j5" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction5()" name="j5" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction5()" name="j5" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction6()" name="j6" value="a"/>Data Jawaban a: 6</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction6()" name="j6" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction6()" name="j6" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction6()" name="j6" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction6()" name="j6" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction7()" name="j7" value="a"/>Data Jawaban a: 7</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction7()" name="j7" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction7()" name="j7" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction7()" name="j7" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction7()" name="j7" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction8()" name="j8" value="a"/>Data Jawaban a: 8</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction8()" name="j8" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction8()" name="j8" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction8()" name="j8" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction8()" name="j8" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction9()" name="j9" value="a"/>Data Jawaban a: 9</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction9()" name="j9" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction9()" name="j9" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction9()" name="j9" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction9()" name="j9" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction10()" name="j10" value="a"/>Data Jawaban a: 9</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction10()" name="j10" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction10()" name="j10" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction10()" name="j10" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction10()" name="j10" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction11()" name="j11" value="a"/>Data Jawaban a: 1</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction11()" name="j11" value="b"/>Data Jawaban b: 1</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction11()" name="j11" value="c"/>Data Jawaban c: 1</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction11()" name="j11" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction11()" name="j11" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction12()" name="j12" value="a"/>Data Jawaban a: 2</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction12()" name="j12" value="b"/>Data Jawaban b: 2</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction12()" name="j12" value="c"/>Data Jawaban c: 2</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction12()" name="j12" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction12()" name="j12" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction13()" name="j13" value="a"/>Data Jawaban a: 3</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction13()" name="j13" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction13()" name="j13" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction13()" name="j13" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction13()" name="j13" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction14()" name="j14" value="a"/>Data Jawaban a: 4</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction14()" name="j14" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction14()" name="j14" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction14()" name="j14" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction14()" name="j14" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction15()" name="j15" value="a"/>Data Jawaban a: 5</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction15()" name="j15" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction15()" name="j15" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction15()" name="j15" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction15()" name="j15" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction16()" name="j16" value="a"/>Data Jawaban a: 6</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction16()" name="j16" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction16()" name="j16" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction16()" name="j16" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction16()" name="j16" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction17()" name="j17" value="a"/>Data Jawaban a: 7</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction17()" name="j17" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction17()" name="j17" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction17()" name="j17" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction17()" name="j17" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction18()" name="j18" value="a"/>Data Jawaban a: 8</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction18()" name="j18" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction18()" name="j18" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction18()" name="j18" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction18()" name="j18" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction19()" name="j19" value="a"/>Data Jawaban a: 9</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction19()" name="j19" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction19()" name="j19" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction19()" name="j19" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction19()" name="j19" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction20()" name="j20" value="a"/>Data Jawaban a: 9</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction20()" name="j20" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction20()" name="j20" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction20()" name="j20" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction20()" name="j20" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction21()" name="j21" value="a"/>Data Jawaban a: 1</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction21()" name="j21" value="b"/>Data Jawaban b: 1</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction21()" name="j21" value="c"/>Data Jawaban c: 1</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction21()" name="j21" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction21()" name="j21" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction22()" name="j22" value="a"/>Data Jawaban a: 2</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction22()" name="j22" value="b"/>Data Jawaban b: 2</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction22()" name="j22" value="c"/>Data Jawaban c: 2</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction22()" name="j22" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction22()" name="j22" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction23()" name="j23" value="a"/>Data Jawaban a: 3</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction23()" name="j23" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction23()" name="j23" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction23()" name="j23" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction23()" name="j23" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction24()" name="j24" value="a"/>Data Jawaban a: 4</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction24()" name="j24" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction24()" name="j24" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction24()" name="j24" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction24()" name="j24" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction25()" name="j25" value="a"/>Data Jawaban a: 5</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction25()" name="j25" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction25()" name="j25" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction25()" name="j25" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction25()" name="j25" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction26()" name="j26" value="a"/>Data Jawaban a: 6</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction26()" name="j26" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction26()" name="j26" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction26()" name="j26" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction26()" name="j26" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction27()" name="j27" value="a"/>Data Jawaban a: 7</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction27()" name="j27" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction27()" name="j27" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction27()" name="j27" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction27()" name="j27" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction28()" name="j28" value="a"/>Data Jawaban a: 8</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction28()" name="j28" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction28()" name="j28" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction28()" name="j28" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction28()" name="j28" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction29()" name="j29" value="a"/>Data Jawaban a: 9</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction29()" name="j29" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction29()" name="j29" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction29()" name="j29" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction29()" name="j29" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction30()" name="j30" value="a"/>Data Jawaban a: 9</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction30()" name="j30" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction30()" name="j30" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction30()" name="j30" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction30()" name="j30" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction31()" name="j31" value="a"/>Data Jawaban a: 1</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction31()" name="j31" value="b"/>Data Jawaban b: 1</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction31()" name="j31" value="c"/>Data Jawaban c: 1</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction31()" name="j31" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction31()" name="j31" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction32()" name="j32" value="a"/>Data Jawaban a: 2</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction32()" name="j32" value="b"/>Data Jawaban b: 2</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction32()" name="j32" value="c"/>Data Jawaban c: 2</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction32()" name="j32" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction32()" name="j32" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction33()" name="j33" value="a"/>Data Jawaban a: 3</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction33()" name="j33" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction33()" name="j33" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction33()" name="j33" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction33()" name="j33" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction34()" name="j34" value="a"/>Data Jawaban a: 4</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction34()" name="j34" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction34()" name="j34" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction34()" name="j34" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction34()" name="j34" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction35()" name="j35" value="a"/>Data Jawaban a: 5</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction35()" name="j35" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction35()" name="j35" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction35()" name="j35" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction35()" name="j35" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction36()" name="j36" value="a"/>Data Jawaban a: 6</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction36()" name="j36" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction36()" name="j36" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction36()" name="j36" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction36()" name="j36" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction37()" name="j37" value="a"/>Data Jawaban a: 7</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction37()" name="j37" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction37()" name="j37" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction37()" name="j37" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction37()" name="j37" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction38()" name="j38" value="a"/>Data Jawaban a: 8</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction38()" name="j38" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction38()" name="j38" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction38()" name="j38" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction38()" name="j38" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction39()" name="j39" value="a"/>Data Jawaban a: 9</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction39()" name="j39" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction39()" name="j39" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction39()" name="j39" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction39()" name="j39" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction40()" name="j40" value="a"/>Data Jawaban a: 9</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction40()" name="j40" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction40()" name="j40" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction40()" name="j40" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction40()" name="j40" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction41()" name="j41" value="a"/>Data Jawaban a: 1</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction41()" name="j41" value="b"/>Data Jawaban b: 1</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction41()" name="j41" value="c"/>Data Jawaban c: 1</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction41()" name="j41" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction41()" name="j41" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction42()" name="j42" value="a"/>Data Jawaban a: 2</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction42()" name="j42" value="b"/>Data Jawaban b: 2</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction42()" name="j42" value="c"/>Data Jawaban c: 2</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction42()" name="j42" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction42()" name="j42" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction43()" name="j43" value="a"/>Data Jawaban a: 3</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction43()" name="j43" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction43()" name="j43" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction43()" name="j43" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction43()" name="j43" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction44()" name="j44" value="a"/>Data Jawaban a: 4</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction44()" name="j44" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction44()" name="j44" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction44()" name="j44" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction44()" name="j44" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction45()" name="j45" value="a"/>Data Jawaban a: 5</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction45()" name="j45" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction45()" name="j45" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction45()" name="j45" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction45()" name="j45" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction46()" name="j46" value="a"/>Data Jawaban a: 6</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction46()" name="j46" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction46()" name="j46" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction46()" name="j46" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction46()" name="j46" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction47()" name="j47" value="a"/>Data Jawaban a: 7</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction47()" name="j47" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction47()" name="j47" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction47()" name="j47" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction47()" name="j47" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction48()" name="j48" value="a"/>Data Jawaban a: 8</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction48()" name="j48" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction48()" name="j48" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction48()" name="j48" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction48()" name="j48" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction49()" name="j49" value="a"/>Data Jawaban a: 9</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction49()" name="j49" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction49()" name="j49" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction49()" name="j49" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction49()" name="j49" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
							<li>
								<div id="slidec">
								<table width="100%">
									<tr>
										<td><input type="radio" onclick="myFunction50()" name="j50" value="a"/>Data Jawaban a: 9</td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction50()" name="j50" value="b"/>Data Jawaban b: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction50()" name="j50" value="c"/>Data Jawaban c: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction50()" name="j50" value="d"/>Data Jawaban d: </td>
									</tr>
									<tr>
										<td><input type="radio" onclick="myFunction50()" name="j50" value="e"/>Data Jawaban e: </td>
									</tr>
								</table>
								</div>
							</li>
						</ul>
					</div>
				</div>
				</form>
				<div class="custom-navigation wrp">
					<table>
						<tr>
						<td class="td-wrp"> <div class="btn-tandai text-b" onclick="tandai()" id="tandai">Tandai</div></td>
						<td class="td-wrp"> <div class="btn-hapus text-b" onclick="hpstandai()" id="hpstandai">Hapus Tanda</div></td>
						<td class="td-wrp"> <a href="#" class="flex-prev"> <div class="btn-np text-b"><< Sebelumnya</div></a></td>
						<td class="td-wrp"> <a href="#" class="flex-next"> <div class="btn-np text-b">Selanjutnya >></div></a></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-4 r">
				<div class="numb">
					<div class="flexslider-control" id="nav-slider">
						<ul class="slides">
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
							<li id="active"></li>
						</ul>
					</div>

					<div class="slide_controll">
						<div class="tbl-container">
							<table class="r" width="100%">
								<tr>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo1">1</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo2">2</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo3">3</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo4">4</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo5">5</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo6">6</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo7">7</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo8">8</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo9">9</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo10">10</div></a></td>
								</tr>
								<tr>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo11">11</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo12">12</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo13">13</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo14">14</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo15">15</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo16">16</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo17">17</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo18">18</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo19">19</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo20">20</div></a></td>
								</tr>
								<tr>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo21">21</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo22">22</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo23">23</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo24">24</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo25">25</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo26">26</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo27">27</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo28">28</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo29">29</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo30">30</div></a></td>
								</tr>
								<tr>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo31">31</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo32">32</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo33">33</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo34">34</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo35">35</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo36">36</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo37">37</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo38">38</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo39">39</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo40">40</div></a></td>
								</tr>
								<tr>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo41">41</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo42">42</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo43">43</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo44">44</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo45">45</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo46">46</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo47">47</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo48">48</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo49">49</div></a></td>
								<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo50">50</div></a></td>
								</tr>
							</table>
						</div>
					</div>
					<div class="button-kirim r">
					<a href="#"><div class="text-b-kirim" onclick="konfirmasi()" id="hasil">Kirim Jawaban</div></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>
	

	<script>
	var children_slides = $('.flexslider_children').flexslider({
	animation: "slide",
	slideshow : false, // Remove the animations
	controlNav : false // Remove the controls
	}); 

	/** 
	* Set up the main flexslider
	*/
	$('.flexslider').flexslider({
	animation: "slide",
	pauseOnHover : true,
	slideshow:false,
	controlsContainer: $(".custom-controls-container"),
	customDirectionNav: $(".custom-navigation a"),
  // Call the update_children_slides which itterates through all children slides 

        'before' : function(slider){ // Hijack the flexslider
			$('#slider .flex-direction-nav').css({visibility:'hidden'});
    		update_children_slides(slider.animatingTo);
		}   
	}); 
	/** 
	* Method that updates children slides
	* fortunately, since all the children are not animating,
	* they will only update if the main flexslider updates. 
	*/

	function update_children_slides (slide_number){
	// Iterate through the children slides but not past the max
		for (i=0;i<children_slides.length;i++) {
			// Run the animate method on the child slide
			$(children_slides[i]).data('flexslider').flexAnimate(slide_number);
		}   
	}

	$(function() {
		$('#main-slider').flexslider({
			animation: "slide",
			slideshow: false,
			directionNav: false,
			controlNav: false
		});

		$('.flexslider-control').flexslider({
			directionNav: false,
			slideshow: false,
			sync: '#main-slider',
			manualControls: ".slide_controll .td-numb",
		});
	});

	// BUTTON TANDAI
	function tandai() {
	var elementg = document.querySelector('.numb-g');
	var index_active = $('li:has(.flex-active)').index('.flex-control-nav li')+1;
	var demo = document.getElementById("demo"+index_active);

		demo.classList.add("numb-y");
	}

	// BUTTON HAPUS TANDA
	function hpstandai() {
	var elementg = document.querySelector('.numb-g');
	var index_active = $('li:has(.flex-active)').index('.flex-control-nav li')+1;
	var demo = document.getElementById("demo"+index_active);
		demo.classList.remove("numb-y");
	}

	// BUTTON SUDAH DIJAWAB
	function myFunction() {
	var frame = document.getElementById("demo1");
		frame.classList.add("numb-g");
	}

	function myFunction2() {
	var frame = document.getElementById("demo2");
			frame.classList.add("numb-g");
	}

	function myFunction3() {
	var frame = document.getElementById("demo3");
			frame.classList.add("numb-g");
	}

	function myFunction4() {
	var frame = document.getElementById("demo4");
			frame.classList.add("numb-g");
	}
	function myFunction5() {
	var frame = document.getElementById("demo5");
			frame.classList.add("numb-g");
	}
	function myFunction6() {
	var frame = document.getElementById("demo6");
			frame.classList.add("numb-g");
	}
	function myFunction7() {
	var frame = document.getElementById("demo7");
			frame.classList.add("numb-g");
	}
	function myFunction8() {
	var frame = document.getElementById("demo8");
			frame.classList.add("numb-g");
	}
	function myFunction9() {
	var frame = document.getElementById("demo9");
			frame.classList.add("numb-g");
	}
	function myFunction10() {
	var frame = document.getElementById("demo10");
			frame.classList.add("numb-g");
	}
	function myFunction11() {
	var frame = document.getElementById("demo11");
			frame.classList.add("numb-g");
	}
	function myFunction12() {
	var frame = document.getElementById("demo12");
			frame.classList.add("numb-g");
	}
	function myFunction13() {
	var frame = document.getElementById("demo13");
			frame.classList.add("numb-g");
	}
	function myFunction14() {
	var frame = document.getElementById("demo14");
			frame.classList.add("numb-g");
	}
	function myFunction15() {
	var frame = document.getElementById("demo15");
			frame.classList.add("numb-g");
	}
	function myFunction16() {
	var frame = document.getElementById("demo16");
			frame.classList.add("numb-g");
	}
	function myFunction17() {
	var frame = document.getElementById("demo17");
			frame.classList.add("numb-g");
	}
	function myFunction18() {
	var frame = document.getElementById("demo18");
			frame.classList.add("numb-g");
	}
	function myFunction19() {
	var frame = document.getElementById("demo19");
			frame.classList.add("numb-g");
	}
	function myFunction20() {
	var frame = document.getElementById("demo20");
			frame.classList.add("numb-g");
	}
	function myFunction21() {
	var frame = document.getElementById("demo21");
			frame.classList.add("numb-g");
	}
	function myFunction22() {
	var frame = document.getElementById("demo22");
			frame.classList.add("numb-g");
	}
	function myFunction23() {
	var frame = document.getElementById("demo23");
			frame.classList.add("numb-g");
	}
	function myFunction24() {
	var frame = document.getElementById("demo24");
			frame.classList.add("numb-g");
	}
	function myFunction25() {
	var frame = document.getElementById("demo25");
			frame.classList.add("numb-g");
	}
	function myFunction26() {
	var frame = document.getElementById("demo26");
			frame.classList.add("numb-g");
	}
	function myFunction27() {
	var frame = document.getElementById("demo27");
			frame.classList.add("numb-g");
	}
	function myFunction28() {
	var frame = document.getElementById("demo28");
			frame.classList.add("numb-g");
	}
	function myFunction29() {
	var frame = document.getElementById("demo29");
			frame.classList.add("numb-g");
	}
	function myFunction30() {
	var frame = document.getElementById("demo30");
			frame.classList.add("numb-g");
	}
	function myFunction31() {
	var frame = document.getElementById("demo31");
			frame.classList.add("numb-g");
	}
	function myFunction32() {
	var frame = document.getElementById("demo32");
			frame.classList.add("numb-g");
	}
	function myFunction33() {
	var frame = document.getElementById("demo33");
			frame.classList.add("numb-g");
	}
	function myFunction34() {
	var frame = document.getElementById("demo34");
			frame.classList.add("numb-g");
	}
	function myFunction35() {
	var frame = document.getElementById("demo35");
			frame.classList.add("numb-g");
	}
	function myFunction36() {
	var frame = document.getElementById("demo36");
			frame.classList.add("numb-g");
	}
	function myFunction37() {
	var frame = document.getElementById("demo37");
			frame.classList.add("numb-g");
	}
	function myFunction38() {
	var frame = document.getElementById("demo38");
			frame.classList.add("numb-g");
	}
	function myFunction39() {
	var frame = document.getElementById("demo39");
			frame.classList.add("numb-g");
	}
	function myFunction40() {
	var frame = document.getElementById("demo40");
			frame.classList.add("numb-g");
	}
	function myFunction41() {
	var frame = document.getElementById("demo41");
			frame.classList.add("numb-g");
	}
	function myFunction42() {
	var frame = document.getElementById("demo42");
			frame.classList.add("numb-g");
	}
	function myFunction43() {
	var frame = document.getElementById("demo43");
			frame.classList.add("numb-g");
	}
	function myFunction44() {
	var frame = document.getElementById("demo44");
			frame.classList.add("numb-g");
	}
	function myFunction45() {
	var frame = document.getElementById("demo45");
			frame.classList.add("numb-g");
	}
	function myFunction46() {
	var frame = document.getElementById("demo46");
			frame.classList.add("numb-g");
	}
	function myFunction47() {
	var frame = document.getElementById("demo47");
			frame.classList.add("numb-g");
	}
	function myFunction48() {
	var frame = document.getElementById("demo48");
			frame.classList.add("numb-g");
	}
	function myFunction49() {
	var frame = document.getElementById("demo49");
			frame.classList.add("numb-g");
	}
	function myFunction50() {
	var frame = document.getElementById("demo50");
			frame.classList.add("numb-g");
	}

	function getTimeRemaining(endtime) {
		const total = Date.parse(endtime) - Date.parse(new Date());
		const seconds = Math.floor((total / 1000) % 60);
		const minutes = Math.floor((total / 1000 / 60) % 60);
		const hours = Math.floor((total / (1000 * 60 * 60)) % 24);
		
		return {
			total,
			hours,
			minutes,
			seconds
		};
	}

	function initializeClock(id, endtime) {
	const clock = document.getElementById(id);
	const hoursSpan = clock.querySelector('.hours');
	const minutesSpan = clock.querySelector('.minutes');
	const secondsSpan = clock.querySelector('.seconds');

	function updateClock() {
		const t = getTimeRemaining(endtime);

		hoursSpan.innerHTML = ('0' + t.hours + ':').slice(-3);
		minutesSpan.innerHTML = ('0' + t.minutes + ':').slice(-3);
		secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

		if (t.total <= 0) {
		clearInterval(timeinterval);
		alert("Waktu telah habis, Silahkan tekan tombol OK untuk mengirim jawaban");
		$('#latihan_soal').submit();
		}
	}

	updateClock();
	const timeinterval = setInterval(updateClock, 1000);
	}

	const deadline = new Date(Date.parse(new Date()) + 00 + 100 * 60 * 1000); //90menit
	initializeClock('clockdiv', deadline);

	function konfirmasi(){
		acModal.confirm({
			title: 'Kirim Jawaban',
			message: 'Apakah kamu yakin ingin mengirim jawaban?',
			successCallBack: function () {
				document.getElementById('latihan_soal').submit();
			},
			cancelCallBack: function () {
				text = "Kirim Jawaban";
			},
		});
		document.getElementById("hasil").innerHTML = text;
	};
</script>
</body>
</html>