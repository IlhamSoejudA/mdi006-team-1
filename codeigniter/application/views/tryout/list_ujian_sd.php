<head>
	<title><?php echo $title ?></title>
</head>
<section>
	<div class="wrapper-lu">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="lu-text">
				<center>
					<h3>Paket Ujian Nasional Bahasa Inggris Tingkat SD</h3>
				</center>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<center>
				<div class="img-lu">
						<img src="assets/sd.svg"/>
				</div>
				</center>
			</div>
		</div>
	</div>
	</div>
</section>
<section>
	<div class="wrapper-lu2">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="table-lu">
					<table>
						<tr>
						<th>Paket UN 2015 - A</th>
						</tr>
						<tr>
						<td class="td-detail">Jumlah Soal : 50</td>
						</tr>
						<tr>
						<td class="td-detail">Durasi : 90 menit</td>
						</tr>
						<tr>
						<td class="td-start"><a href="<?php echo base_url('mulai_ujian') ?>">Kerjakan</a></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="table-lu">
					<table>
						<tr>
						<th>Paket UN 2015 - B</th>
						</tr>
						<tr>
						<td class="td-detail">Jumlah Soal : 50</td>
						</tr>
						<tr>
						<td class="td-detail">Durasi : 90 menit</td>
						</tr>
						<tr>
						<td class="td-start"><a href="<?php echo base_url('mulai_ujian') ?>" onClick="alert('Bersiap Ujian!')">Kerjakan</a></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="table-lu">
					<table>
						<tr>
						<th>Paket UN 2016 - A</th>
						</tr>
						<tr>
						<td class="td-detail">Jumlah Soal : 50</td>
						</tr>
						<tr>
						<td class="td-detail">Durasi : 90 menit</td>
						</tr>
						<tr>
						<td class="td-start"><a href="<?php echo base_url('mulai_ujian') ?>">Kerjakan</a></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-3">
				<div class="table-lu">
					<table>
						<tr>
						<th>Paket UN 2016 - B</th>
						</tr>
						<tr>
						<td class="td-detail">Jumlah Soal : 50</td>
						</tr>
						<tr>
						<td class="td-detail">Durasi : 90 menit</td>
						</tr>
						<tr>
						<td class="td-start"><a href="<?php echo base_url('mulai_ujian') ?>">Kerjakan</a></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>
	
</body>
</html>