<head>
	<title><?php echo $title ?></title>
</head>
<section>
	<div class="wrapper-lu">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="lu-text">
				<center>
                <?php 
                foreach ($data as $grade){
                    if($grade["id_grade"] == $id_grade){
						foreach ($grade["majors"] as $mapel){
							if($mapel["majors_id"] == $id_major){
                ?>
               
					<h3>Paket Ujian Nasional <?php echo $mapel['majors_name'];?> Tingkat <?php echo $grade["grade"];?></h3>
						<?php }} ?>
				</center>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<center>
				<div class="img-lu">
				<?php 
					if($grade["grade"] == 'SD'){
						$gbr = base_url("assets/sd.svg"); 
					}
					else if($grade["grade"] == 'SMP'){
						$gbr = base_url("assets/smp.svg"); 
					}
					else if($grade["grade"] == 'SMA'){
						$gbr = base_url("assets/sma.svg");
					}
					?>
						<img src="<?php echo $gbr ?>"/>
				</div>
				</center>
			</div>
		</div>
	</div>
	</div>
</section>
<section>
	<div class="wrapper-lu2">
	<div class="container">
		<div class="row">
            <?php 
				foreach ($grade["majors"] as $mapel){
                    if($mapel["majors_id"] == $id_major){
                        foreach ($mapel["exams"] as $list){
			?>
			<div class="col-md-3">
				<div class="table-lu">
					<table>
						<tr>
						<th><?php echo $list['exams_name'];?></th>
						</tr>
						<tr>
						<td class="td-detail">Jumlah Soal : <?php echo $list['total'];?></td>
						</tr>
						<tr>
						<td class="td-detail">Durasi : <?php echo $list['duration'];?> menit</td>
						</tr>
						<tr>
                        <td class="td-start"><a href="<?php echo base_url('event/ujian/'.$list['exams_id']) ?>" name="start_exam">Kerjakan</a></td>
						</tr>
					</table>
				</div>
            </div>
            <?php }}} ?>
		</div>
	</div>
    </div>
    <?php }} ?>
</section>
	
</body>
</html>