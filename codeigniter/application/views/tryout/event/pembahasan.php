<head>
	<title><?php echo $title ?></title>
</head>
<?php 
foreach ($data as $pembahasan){
	if($pembahasan["id_paket"] == $id_exams){
?>
<section>
	<div class="wrapper-hp">
			<div class="bg-hp">
				<img src="<?php echo base_url("/assets/pembahasan.png") ?>" width="100%" height="266px">
			</div>									
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<center>
						<div class="text-hp">
							<h3>PEMBAHASAN</h3>
						</div>
					</center>
				</div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="wrapper-hp2">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="numb">
						<div class="flexslider-control" id="nav-slider">
							<ul class="slides">
							<?php 
								$nomor = 0;
								foreach ($pembahasan["pembahasan"] as $answer){
									foreach ($answer["ans"] as $ans){
								$nomor ++;
							?>
								<li id="active"></li>
							<?php }} ?>
							</ul>
						</div>
							<div class="col-md-12 col-sm-12">
								<div class="slide_controll">
									<table width="100%">
										<tr>
										<?php 
											$nomor = 1;
											$baris = 1;
											foreach ($pembahasan["pembahasan"] as $answer){
												foreach ($answer["ans"] as $ans){
												if($answer['jawaban_siswa'] == $answer['jawaban_benar']){
													$cek = 'numb-t'; 
												}
												else{
													$cek = 'numb-f'; 
												}
												if($baris == 12){
													$baris=0;
													
										?>
											<td class="td-numbp" ><a href="#"><div class="<?php echo $cek ?> text-no" ><?php echo $nomor; ?></div></a></td>
										</tr>
										<?php } else{ ?>
											<td class="td-numbp" ><a href="#"><div class="<?php echo $cek ?> text-no" ><?php echo $nomor; ?></div></a></td>
										<?php } ?>
										<?php $nomor++; $baris++; }} ?>	
										</tr>
									</table>
								</div>
							</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div id="main-slider" class="flexslider">
						<ul class="slides">
							<?php 
								$nomor = 0;
								foreach ($pembahasan["pembahasan"] as $bahas){ 
								$nomor ++;
							?>
							<li>
								<?php 
									if($bahas['jawaban_siswa'] == $bahas['jawaban_benar']){
										$tag = 'ps-tag-true'; 
									}
									else{
										$tag = 'ps-tag-false'; 
									}
								?>
								<div class="<?php echo $tag ?>">
									<center>
										<p><?php echo $bahas['category'];?></p>
									</center>
								</div>
								<div class="frame-pembahasan">
									<div class="row">
										<div class="col-md-12 col-sm-12">
											<div class="ps-icon r">
											<center>
											<?php 
												if($bahas['jawaban_siswa'] == $bahas['jawaban_benar']){
													$icon = base_url("assets/true.svg"); 
												}
												else{
													$icon = base_url("assets/false.svg"); 
												}
											?>
												<img src="<?php echo $icon ?>" class="icon-ft">
											</center>
											</div>
										</div>
										<div class="col-md-12 col-sm-12">
											<div class="ps-soal">
												<p>Soal <?php echo $bahas['id_pembahasan'];?>)</p>
												<p><?php echo $bahas['question'];?></p>
											</div>
										</div>
										<div class="col-md-12 col-s-12">
											<div class="pd-jawaban">
												<?php
													foreach ($bahas["ans"] as $ans){
												?>
												<table width="100%">
												<?php foreach ($ans["data_answers"] as $tans){ ?>
													<tr>
													<?php 
														if($tans['ans'] == $bahas['jawaban_siswa']){
															$status = 'checked'; 
														}
														else{
															$status = ''; 
														}
													?>
														<td><input type="radio" name="<?php echo $ans['answer_id']; ?>" value="<?php echo $tans['ans'];?>" <?php echo $status ?>/><?php echo $tans['ans'];?></td>
													</tr>
													<?php } ?>
												</table>
												<?php } ?>
											</div>
										</div>
										<div class="col-md-12 col-sm-12">
											<div class="ps-pembahasan">
												<h4>Pembahasan:</h4>
												<p><?php echo $bahas['pembahasan'];?></p>
											</div>
										</div>
								</li>
								<?php } ?>
							</ul>
						</div>
						<div class="custom-navigation wrp">
								<table>
									<tr>
									<td class="td-wrp"> <a href="#" class="flex-prev"> <div class="btn-np text-b"><< Sebelumnya</div></a></td>
									<td class="td-wrp"> <a href="#" class="flex-next"> <div class="btn-np text-b">Selanjutnya >></div></a></td>
									</tr>
								</table>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script>

	$('.flexslider').flexslider({
	animation: "slide",
	pauseOnHover : true,
	autoHeight : true,
	slideshow:false,
    animationLoop: false,
    pauseOnHover: true,
    smoothHeight: true,
    itemMargin: 0,
    minItems: 1,
    maxItems: 1,
	controlsContainer: $(".custom-controls-container"),
	customDirectionNav: $(".custom-navigation a"),
  // Call the update_children_slides which itterates through all children slides 

	}); 
	/** 
	* Method that updates children slides
	* fortunately, since all the children are not animating,
	* they will only update if the main flexslider updates. 
	*/
	$(function() {
		$('#main-slider').flexslider({
			animation: "slide",
			slideshow: false,
			directionNav: false,
			controlNav: false
		});

		$('.flexslider-control').flexslider({
			directionNav: false,
			slideshow: false,
			sync: '#main-slider',
			manualControls: ".slide_controll .td-numbp",
		});
	});


</script>
<?php }} ?>
</body>
</html>