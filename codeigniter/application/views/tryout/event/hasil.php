<head>
	<title><?php echo $title ?></title>
</head>
<?php
foreach ($data as $hasil){
	if($hasil["id_siswa"] == $id_student){
?>
<section>
	<div class="wrapper-hu">
			<div class="bg-hu">
				<img src="<?php echo base_url("/assets/hasil.png") ?>" width="100%" height="366px">
			</div>									
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<center>
						<div class="text-hu">
							<h3>Hai <?php echo $hasil['nama_siswa'];?> . . . </h3></br>
							<p>Terimakasih sudah menyelesaikan Try Out dengan sungguh-sungguh</p>
						</div>
					</center>
				</div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="wrapper-hu2">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="text-hu2">
				<center>
					<h4>Hasil Latihan Soal</h4>
				</center>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="table-hu">
					<table align="center">
						<tr>
						<td class="td-data">Mata Pelajaran</td>
						<td class="td-data"><?php echo $hasil['mata_pelajaran'];?></td>
						</tr>
						<tr>
						<td class="td-data">Paket Soal</td>
						<td class="td-data"><?php echo $hasil['paket_soal'];?></td>
						</tr>
						<tr>
						<td class="td-data">Waktu Pengerjaan</td>
						<td class="td-data"><?php echo $hasil['waktu_pengerjaan'];?></td>
						</tr>
						<tr>
						<td class="td-data">Jawaban Benar</td>
						<td class="td-data"><?php echo $hasil['jawaban_benar'];?></td>
						</tr>
						<tr>
						<td class="td-data">Jawaban Salah</td>
						<td class="td-data"><?php echo $hasil['jawaban_salah'];?></td>
						</tr>
						<tr>
                        <?php 
                            $a = $hasil['jawaban_salah'];
                            $b = $hasil['jawaban_benar'];
                            $soal = $a+$b;
                            $hit_nilai = $b * 100;
                            $nilai = $hit_nilai / $soal;
                        
                        ?>
						<th class="td-data">Nilai</th>
						<th class="td-data"><?php echo $nilai;?></th>
						</tr>
						<tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="btn-hasil r">
					<center>
                    <?php 
					foreach ($ujian as $id_ujian){
						if($id_ujian["exams_id"] == $id_exams){
				    ?>
                        <a href="<?php echo base_url('event/pembahasan/'.$id_ujian['exams_id']) ?>">
                    <?php }} ?>
							<div class="btn-pa"> Lihat Pembahasan</div>
						</a>
					</center>
				</div>
			</div>
			<div class="col-md-6">
				<div class="btn-hasil l">
					<center>
						<?php 
						foreach ($ujian as $id_ujian){
							if($id_ujian["exams_id"] == $id_exams){
						?>
                        <a href="<?php echo base_url('event/analisis/'.$hasil['id_siswa']."/".$id_ujian['exams_id']) ?>">
							<div class="btn-pa"> Lihat Analisis</div>
						<?php }} ?>
						</a>
					</center>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>
    <?php }} ?>
</body>
</html>