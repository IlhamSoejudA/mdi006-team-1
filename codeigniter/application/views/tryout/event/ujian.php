<head>
	<title><?php echo $title ?></title>
</head>
<body>
<section>
<?php 
foreach ($data as $exams){
	if($exams["exams_id"] == $id_exams){
?>
	<div class="wrapper-mu">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-7">
				<div class="mu-text">
					<div class="table-mu">
						<table>
							<tr>
							<td>Tingkat</td>
							<td class="table-mu-td">: <?php echo $exams["grade"];?></td>
							</tr>
							<tr>
							<td>Mata Pelajaran</td>
							<td class="table-mu-td">: <?php echo $exams["major"];?></td>
							</tr>
							<tr>
							<td>Paket Soal</td>
							<td class="table-mu-td">: <?php echo $exams["exams_name"];?></td>
							</tr>
						</table>
					</div>
				</div>
				</div>
				<div class="col-md-2 r">
					<center>
					<div class="mu-clock r">
						<img src="<?php echo base_url("assets/i-clock.svg") ?>"/>  
						<span id="clockdiv"><b></b>
							<span class="hours"></span>
							<span class="minutes"></span>
							<span class="seconds"></span></b>
						</span>
					</div>
					</center>
				</div>
				<div class="col-md-3">
					<div class="mu-text r">
						<div class="table-mu">
							<table>
								<tr>
								<th colspan="2">Keterangan Nomor Soal</th>
								</tr>
								<tr>
								<td><img src="<?php echo base_url("assets/i-green.svg") ?>"/></td>
								<td class="table-mu-td"> Sudah dijawab</td>
								</tr>
								<tr>
								<td><img src="<?php echo base_url("assets/i-black.svg") ?>"/></td>
								<td class="table-mu-td"> Belum dijawab</td>
								</tr>
								<tr>
								<td><img src="<?php echo base_url("assets/i-yellow.svg") ?>"/></td>
								<td class="table-mu-td"> Tandai</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
	</div>
	</div>
</section>
<section>
	<div class="wrapper-mu2">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8">
				<div class="soal">
					<div id="main-slider" class="flexslider">
						<ul class="slides">
						<?php 
						$nomor = 0;
						foreach ($exams["quest"] as $quest){ 
						$nomor ++;
						?>
							<li>
							<div class="btn-nomor text-n">
								<center>
								<?php echo $nomor;?>
								</center>
							</div>
							<p><?php echo $quest['question'];?></p>
							</li>
						<?php } ?>
						</ul>
					</div>
				</div>
				<?php 
					foreach ($id as $id_siswa){
				?>
				<form action="<?php echo base_url('event/hasil/'.$id_siswa['id_siswa']."/".$exams['exams_id']) ?>" method="post" name="latihan_soal" id="latihan_soal">
					<?php } ?>
				<div class="jawaban">
					<div class="flexslider_children">
						<ul class="slides">
						<?php 
							$nomor = 0;
							foreach ($exams["quest"] as $quest){ 
							foreach ($exams["ans"] as $ans){ 
							$nomor ++;
							if($quest['question_id'] == $ans['answer_id']){
						?>
							<li>
								<table width="100%">
									
									<?php 
										$numb = 0;
										foreach ($ans["data_answers"] as $tans){
										$numb++;
									?>
									<tr>
										<td><input type="radio" onclick="myFunction()" name="<?php echo $quest['question_id']; ?>" value="<?php echo $tans['ans'];?>" /><?php echo $tans['ans'];?></td>
									</tr>
									<?php } ?>
								</table>
							</li>
							<?php }}} ?>
						</ul>
					</div>
				</div>
				</form>
				<div class="custom-navigation wrp">
					<table>
						<tr>
						<td class="td-wrp"> <div class="btn-tandai text-b" onclick="tandai()" id="tandai">Tandai</div></td>
						<td class="td-wrp"> <div class="btn-hapus text-b" onclick="hpstandai()" id="hpstandai">Hapus Tanda</div></td>
						<td class="td-wrp"> <a href="#" class="flex-prev"> <div class="btn-np text-b"><< Sebelumnya</div></a></td>
						<td class="td-wrp"> <a href="#" class="flex-next"> <div class="btn-np text-b">Selanjutnya >></div></a></td>
						</tr>
					</table>
				</div>
				</div>
			
			<div class="col-md-4 r">
				<div class="numb">
					<div class="flexslider-control" id="nav-slider">
						<ul class="slides">
						<?php 
							$nomor = 0;
							foreach ($exams["ans"] as $ans){ 
							$nomor ++;
						?>
							<li id="active"></li>
						<?php } ?>
						</ul>
					</div>
					<div class="slide_controll">
						<div class="tbl-container">
							<table class="r" width="100%">
								<tr>
								<?php 
									$nomor = 1;
									$baris = 1;
									foreach ($exams["ans"] as $ans){
										if($baris == 10){
											$baris=0;
								?>
									<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo<?php echo $nomor;?>"><?php echo $nomor; ?></div></a></td>
								</tr>
								<?php } else{ ?>
									<td class="td-numb" ><a href="#"><div class="numb-b text-n" id="demo<?php echo $nomor;?>"><?php echo $nomor; ?></div></a></td>
								
								<?php } ?>
								<?php $nomor++; $baris++; } ?>								 
								</tr>
							</table>
						</div>
					</div>
					<div class="button-kirim r">
					<a href="#"><div class="text-b-kirim" onclick="konfirmasi()" id="hasil">Kirim Jawaban</div></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>
	

	<script>
	var children_slides = $('.flexslider_children').flexslider({
	animation: "slide",
	slideshow : false, // Remove the animations
	controlNav : false // Remove the controls
	}); 

	/** 
	* Set up the main flexslider
	*/
	$('.flexslider').flexslider({
	animation: "slide",
	pauseOnHover : true,
	slideshow:false,
	controlsContainer: $(".custom-controls-container"),
	customDirectionNav: $(".custom-navigation a"),
  // Call the update_children_slides which itterates through all children slides 

        'before' : function(slider){ // Hijack the flexslider
			$('#slider .flex-direction-nav').css({visibility:'hidden'});
    		update_children_slides(slider.animatingTo);
		}   
	}); 
	/** 
	* Method that updates children slides
	* fortunately, since all the children are not animating,
	* they will only update if the main flexslider updates. 
	*/

	function update_children_slides (slide_number){
	// Iterate through the children slides but not past the max
		for (i=0;i<children_slides.length;i++) {
			// Run the animate method on the child slide
			$(children_slides[i]).data('flexslider').flexAnimate(slide_number);
		}   
	}

	$(function() {
		$('#main-slider').flexslider({
			animation: "slide",
			slideshow: false,
			directionNav: false,
			controlNav: false
		});

		$('.flexslider-control').flexslider({
			directionNav: false,
			slideshow: false,
			sync: '#main-slider',
			manualControls: ".slide_controll .td-numb",
		});
	});

	// BUTTON TANDAI
	function tandai() {
	var elementg = document.querySelector('.numb-g');
	var index_active = $('li:has(.flex-active)').index('.flex-control-nav li')+1;
	var demo = document.getElementById("demo"+index_active);

		demo.classList.add("numb-y");
	}

	// BUTTON HAPUS TANDA
	function hpstandai() {
	var elementg = document.querySelector('.numb-g');
	var index_active = $('li:has(.flex-active)').index('.flex-control-nav li')+1;
	var demo = document.getElementById("demo"+index_active);
		demo.classList.remove("numb-y");
	}

	// BUTTON SUDAH DIJAWAB
	function myFunction() {
		var elementg = document.querySelector('.numb-g');
		var index_active = $('li:has(.flex-active)').index('.flex-control-nav li')+1;
		var frame = document.getElementById("demo"+index_active);
		frame.classList.add("numb-g");
	}


	function getTimeRemaining(endtime) {
		const total = Date.parse(endtime) - Date.parse(new Date());
		const seconds = Math.floor((total / 1000) % 60);
		const minutes = Math.floor((total / 1000 / 60) % 60);
		const hours = Math.floor((total / (1000 * 60 * 60)) % 24);
		
		return {
			total,
			hours,
			minutes,
			seconds
		};
	}

	function initializeClock(id, endtime) {
	const clock = document.getElementById(id);
	const hoursSpan = clock.querySelector('.hours');
	const minutesSpan = clock.querySelector('.minutes');
	const secondsSpan = clock.querySelector('.seconds');

	function updateClock() {
		const t = getTimeRemaining(endtime);

		hoursSpan.innerHTML = ('0' + t.hours + ':').slice(-3);
		minutesSpan.innerHTML = ('0' + t.minutes + ':').slice(-3);
		secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

		if (t.total <= 0) {
		clearInterval(timeinterval);
		alert("Waktu telah habis, Silahkan tekan tombol OK untuk mengirim jawaban");
		$('#latihan_soal').submit();
		}
	}

	updateClock();
	const timeinterval = setInterval(updateClock, 1000);
	}
    var durasi = <?php echo $exams["duration"];?>;
	const deadline = new Date(Date.parse(new Date()) + 00 + durasi * 60 * 1000); //90menit
	initializeClock('clockdiv', deadline);

	function konfirmasi(){
		acModal.confirm({
			title: 'Kirim Jawaban',
			message: 'Apakah kamu yakin ingin mengirim jawaban?',
			successCallBack: function () {
				document.getElementById('latihan_soal').submit();
			},
			cancelCallBack: function () {
				text = "Kirim Jawaban";
			},
		});
		document.getElementById("hasil").innerHTML = text;
	};
</script>
<?php }} ?>
</body>
</html>