<head>
	<title><?php echo $title ?></title>
</head>
<?php 
foreach ($data as $analisis){
	if($analisis["id_siswa"] == $id_student){ 
?>
<section>
	<div class="wrapper-hu">
			<div class="bg-hu">
				<img src="<?php echo base_url("/assets/banner.jpg") ?>" width="100%" height="366px">
			</div>									
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<center>
						<div class="text-ha">
							<h3>Hai <?php echo $analisis['nama_siswa'];?> . . . </h3></br>
							<p>Kamu adalah siswa yang memiliki ingatan kuat tapi kamu perlu meningkatkan kemampuan analisismu</p>
						</div>
					</center>
				</div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="wrapper-hu2">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="text-hu2">
				<center>
					Analisis Kemampuan
				</center>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="table-hu">
					<table align="center">
						<tr>
						<td class="td-data">Kemampuan menghafal/mengingat (C1)</td>
						<td class="td-data"><?php echo $analisis['c1'];?></td>
						</tr>
						<tr>
						<td class="td-data">Kemampuan memahami (C2)</td>
						<td class="td-data"><?php echo $analisis['c2'];?></td>
						</tr>
						<tr>
						<td class="td-data">Kemampuan menerapkan (C3)</td>
						<td class="td-data"><?php echo $analisis['c3'];?></td>
						</tr>
						<tr>
						<td class="td-data">Kemampuan menganalisis (C4)</td>
						<td class="td-data"><?php echo $analisis['c4'];?></td>
						</tr>
						<tr>
						<td class="td-data">Kemampuan sintesis (C5)</td>
						<td class="td-data"><?php echo $analisis['c5'];?></td>
						</tr>
						<tr>
						<th class="td-data">Kemampuan mengevaluasi (C6)</th>
						<th class="td-data"><?php echo $analisis['c6'];?></th>
						</tr>
						<tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="btn-hasil r">
					<center>
					<?php 
						foreach ($ujian as $id_ujian){
							if($id_ujian["exams_id"] == $id_exams){
						?>
                        <a href="<?php echo base_url('event/hasil/'.$analisis['id_siswa']."/".$id_ujian['exams_id']) ?>">
						<?php }} ?>
							<div class="btn-pa">Kembali</div>
						</a>
					</center>
				</div>
			</div>
			<div class="col-md-6">
				<div class="btn-hasil l">
					<center>
                   <?php 
					foreach ($ujian as $id_ujian){
						if($id_ujian["exams_id"] == $id_exams){
				    ?>
                        <a href="<?php echo base_url('event/pembahasan/'.$id_ujian['exams_id']) ?>">
                    <?php }} ?>
							<div class="btn-pa">Lihat Pembahasan</div>
						</a>
					</center>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>
<?php }} ?>
</body>
</html>