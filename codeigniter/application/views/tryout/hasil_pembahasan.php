<head>
	<title><?php echo $title ?></title>
</head>
<section>
	<div class="wrapper-hp">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-7 col-sm-7">
					<img src="assets/pembahasan.svg" class="img-hp"/>
				</div>
				<div class="col-md-5 col-sm-5">
					<div class="text-hp">
						<p>PEMBAHASAN</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="wrapper-hp2">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="numb">
						<div class="flexslider-control" id="nav-slider">
							<ul class="slides">
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
								<li></li>
							</ul>
						</div>
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<div class="slide_controll">
									<table width="100%">
										<tr>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo1">1</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo2">2</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo3">3</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo4">4</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo5">5</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo6">6</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo7">7</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo8">8</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo9">9</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo10">10</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo11">11</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo12">12</div></a></td>
										</tr>
										<tr>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo13">13</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo14">14</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo15">15</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo16">16</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo17">17</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo18">18</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo19">19</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo20">20</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo21">21</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo22">22</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo23">23</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo24">24</div></a></td>
										</tr>
										<tr>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo25">25</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo26">26</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo27">27</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo28">28</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo29">29</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo30">30</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo31">31</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo32">32</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo33">33</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo34">34</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo35">35</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo36">36</div></a></td>
										</tr>
										<tr>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo37">37</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo38">38</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo39">39</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo40">40</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo41">41</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo42">42</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo43">43</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo44">44</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo45">45</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo46">46</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo47">47</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo48">48</div></a></td>
										</tr>
										<tr>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo49">49</div></a></td>
										<td class="td-numbp" ><a href="#"><div class="numb-t text-no" id="demo50">50</div></a></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div id="main-slider" class="flexslider">
						<ul class="slides">
							<li>
								<div class="ps-tag-true">
									<center>
										<p>C4</p>
									</center>
								</div>
								<div class="frame-pembahasan">
									<div class="row">
										<div class="col-md-12 col-sm-12">
											<div class="ps-icon r">
											<center>
												<img src="assets/true.svg" class="icon-ft">
											</center>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 col-sm-12">
											<div class="ps-soal">
												<p>Soal 1) Attending Extracurricular Activities is Beneficial Many students 
													may want to be the best at school by studying very hard to get 100 on the 
													core subjects taught at school. Sometimes, they forget to play and interact 
													with other people because they need to study the subjects. After school, 
													they go home and study again. They never do anything other than studying 
													as if their world is only about studying. However, they need to do other 
													activities to develop other skills besides studying such as social skills
												</p>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 col-s-12">
											<div class="ps-jawaban">
												<table width="100%">
													<tr>
														<td><input type="radio" id="toggle" name="j1" value="a"/> Data Jawaban a: 1</td>
													</tr>
													<tr>
														<td><input type="radio" id="toggle" name="j1" value="b"/> Data Jawaban b: </td>
													</tr>
													<tr>
														<td><input type="radio" id="toggle" name="j1" value="c" checked="true"/> Data Jawaban c: </td>
													</tr>
													<tr>
														<td><input type="radio" id="toggle" name="j1" value="d"/> Data Jawaban d: </td>
													</tr>
													<tr>
														<td><input type="radio" id="toggle" name="j1" value="e"/> Data Jawaban e: </td>
													</tr>
												</table>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 col-s-12">
											<div class="ps-pembahasan">
												<h4>Pembahasan:</h4>
												<p>Jawaban tersebut dapat kita peroleh dari membaca judul teks di atas 
													“Attending Extracurricular Activities is Beneficial”. Judul dari teks 
													di atas adalah “Menghadiri Extrakulikuler Bermanfaat”
												</p>
											</div>
										</div>
									</div>
								</li>
								<li>
								<div class="ps-tag-false">
									<center>
										<p>C4</p>
									</center>
								</div>
								<div class="frame-pembahasan">
									<div class="row">
										<div class="col-md-12 col-sm-12">
											<div class="ps-icon r">
											<center>
												<img src="assets/false.svg" class="icon-ft">
											</center>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 col-s-12">
											<div class="ps-soal">
												<p>Soal 2) Attending Extracurricular Activities is Beneficial Many students 
													may want to be the best at school by studying very hard to get 100 on the 
													core subjects taught at school. Sometimes, they forget to play and interact 
													with other people because they need to study the subjects. After school, 
													they go home and study again. They never do anything other than studying 
													as if their world is only about studying. However, they need to do other 
													activities to develop other skills besides studying such as social skills 
													to be able to get successful in facing the real world. One of the ways is 
													joining extracurricular activities. Extracurricular activities give some 
													benefits to students for their development. Some students or parents may 
													worry that if the students join an extracurricular activity, they may get 
													bad scores on the core subjects taught at school. A research from a university 
													in America shows that students who join extracurricular activities at school 
													such as music club or sport club get higher average scores than those who do 
													not join any kind of extracurricular activities (Craft, 2012:64). This statement 
													gives understanding that joining an extracurricular activity will not make the 
													students’ scores lower. Then, Jaspal Shindu in The Jakarta Pos, the Founder and 
												</p>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 col-s-12">
											<div class="ps-jawaban">
												<table width="100%">
													<tr>
														<td><input type="radio" id="toggle" name="j2" value="a"/> Data Jawaban a: 2</td>
													</tr>
													<tr>
														<td><input type="radio" id="toggle" name="j2" value="b" checked="true"/> Data Jawaban b: </td>
													</tr>
													<tr>
														<td><input type="radio" id="toggle" name="j2" value="c"/> Data Jawaban c: </td>
													</tr>
													<tr>
														<td><input type="radio" id="toggle" name="j2" value="d"/> Data Jawaban d: </td>
													</tr>
													<tr>
														<td><input type="radio" id="toggle" name="j2" value="e"/> Data Jawaban e: </td>
													</tr>
												</table>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 col-s-12">
											<div class="ps-pembahasan">
												<h4>Pembahasan:</h4>
												<p>2) Jawaban tersebut dapat kita peroleh dari membaca judul teks di atas
												</p>
											</div>
										</div>
									</div>
								</li>
								<li>
								<div class="ps-tag-true">
									<center>
										<p>C5</p>
									</center>
								</div>
								<div class="frame-pembahasan">
									<div class="row">
										<div class="col-md-12 col-sm-12">
											<div class="ps-icon r">
											<center>
												<img src="assets/true.svg" class="icon-ft">
											</center>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 col-s-12">
											<div class="ps-soal">
												<p>Soal 3) Attending Extracurricular Activities is Beneficial Many students 
												</p>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 col-s-12">
											<div class="ps-jawaban">
												<table width="100%">
													<tr>
														<td><input type="radio" id="toggle" name="j3" value="a" checked="true"/> Data Jawaban a: 3</td>
													</tr>
													<tr>
														<td><input type="radio" id="toggle" name="j3" value="b" /> Data Jawaban b: </td>
													</tr>
													<tr>
														<td><input type="radio" id="toggle" name="j3" value="c"/> Data Jawaban c: </td>
													</tr>
													<tr>
														<td><input type="radio" id="toggle" name="j3" value="d"/> Data Jawaban d: </td>
													</tr>
													<tr>
														<td><input type="radio" id="toggle" name="j3" value="e"/> Data Jawaban e: </td>
													</tr>
												</table>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 col-s-12">
											<div class="ps-pembahasan">
												<h4>Pembahasan:</h4>
												<p>2) Jawaban tersebut dapat kita peroleh dari membaca judul teks di atas
												</p>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
						<div class="custom-navigation wrp">
								<table>
									<tr>
									<td class="td-wrp"> <a href="#" class="flex-prev"> <div class="btn-np text-b"><< Sebelumnya</div></a></td>
									<td class="td-wrp"> <a href="#" class="flex-next"> <div class="btn-np text-b">Selanjutnya >></div></a></td>
									</tr>
								</table>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script>

	$('.flexslider').flexslider({
	animation: "slide",
	pauseOnHover : true,
	autoHeight : true,
	slideshow:false,
    animationLoop: false,
    pauseOnHover: true,
    smoothHeight: true,
    itemMargin: 0,
    minItems: 1,
    maxItems: 1,
	controlsContainer: $(".custom-controls-container"),
	customDirectionNav: $(".custom-navigation a"),
  // Call the update_children_slides which itterates through all children slides 

	}); 
	/** 
	* Method that updates children slides
	* fortunately, since all the children are not animating,
	* they will only update if the main flexslider updates. 
	*/
	$(function() {
		$('#main-slider').flexslider({
			animation: "slide",
			slideshow: false,
			directionNav: false,
			controlNav: false
		});

		$('.flexslider-control').flexslider({
			directionNav: false,
			slideshow: false,
			sync: '#main-slider',
			manualControls: ".slide_controll .td-numbp",
		});
	});


</script>
	
</body>
</html>