<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Mahasiswa extends REST_Controller{
    
    public function __construct(){
        parent::__construct();
        $this->load->model('Mahasiswa_model', 'mhs');

        // $this->methods['index_get']['limit'] = 1000;   
    }

    public function tampilan(){
        $this->load->view('v_mahasiswa');
    }

    public function search(){
        $keyword = $this->input->post('nama');
        $data['mahasiswa'] = $this->mhs->getMahasiswaByNama($keyword);
    }

    public function index_get(){
        
        $this->load->view('v_mahasiswa');
        $nama = $this->get('name');

        $id = $this->get('id');
        if($id === null){
            $mahasiswa = $this->mhs->getMahasiswa();
        }else{
            $mahasiswa = $this->mhs->getMahasiswa($id);
        }
        
        if($mahasiswa){
            $this->response([
                'status' => true,
                'data' => $mahasiswa
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'message' => 'id not found'
            ], REST_Controller::HTTP_NOT_FOUND);
        }

    }

    public function index_delete(){
        $id = $this->delete('id');

        if($id === null){
            $this->response([
            'status' => false,
            'message' => 'provide an id'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }else{
            if($this->mhs->deleteMahasiswa($id) > 0){
                $this->response([
                    'status' => true,
                    'id' => $id,
                    'message' => 'deleted'
                ], REST_Controller::HTTP_OK);
            }else{
                $this->response([
                    'status' => false,
                    'message' => 'id not found'
                ], REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

    public function index_post(){
        $data = [
            'name' => $this->post('name'),
            'address' => $this->post('address'),
            'email' => $this->post('email')
        ];

        if($this->mhs->createMahasiswa($data) > 0){
            $this->response([
                'status' => true,
                'message' => 'success'
            ], REST_Controller::HTTP_CREATED);
        }else{
            $this->response([
                'status' => false,
                'message' => 'failed'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function index_put(){
        $id = $this->put('id');
        $data = [
            'name' => $this->put('name'),
            'address' => $this->put('address'),
            'email' => $this->put('email')
        ];

        if($this->mhs->updateMahasiswa($data, $id) > 0){
            $this->response([
                'status' => true,
                'message' => 'update success'
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'message' => 'update failed'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}