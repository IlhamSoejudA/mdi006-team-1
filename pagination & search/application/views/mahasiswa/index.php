<div class="container">
    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
    <?php if ($this->session->flashdata('flash')) : ?>
    <!-- <div class="row mt-3">
        <div class="col-md-6">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                Data mahasiswa <strong>berhasil</strong> <?= $this->session->flashdata('flash'); ?>.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div> -->
    <?php endif; ?>

    
    <div class="row mt-4">
            <div class="col-md-4">
                <form action="<?= base_url() ?>pagination" method="POST">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Search keyword" aria-label="Recipient's username" aria-describedby="button-addon2" name="keyword" autofocus autocomplete="off">
                        <div class="input-group-append">
                            <input class="btn btn-outline-secondary" type="submit" name="submit"></input>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
            <a href="<?= base_url(); ?>pagination/tambah" class="btn btn-primary">Tambah
                Data Mahasiswa</a>
            </div>
    </div>

    <div class="row mt-">
        <div class="col-md-6">
            <small class="text-muted">Results :<?= $total_rows; ?></small>
            <h3>Daftar Mahasiswa</h3>
            
            <table class="table striped-table">
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th></th>
                </tr>
                <?php if (empty($mahasiswa)) : ?>
                    <tr>
                        
                            <div class="alert alert-danger" role="alert">data mahasiswa tidak ditemukan.
                        
                    </tr>
                </div>
            <?php endif; ?>
                <?php foreach ($mahasiswa as $mhs) : ?>
                <tr>
                    <td><?= ++$start; ?></td>
                    <td><?= $mhs['name']; ?></td>
                    <td>
                        <a href="<?= base_url(); ?>pagination/hapus/<?= $mhs['id']; ?>"
                        class="badge badge-danger float-right tombol-hapus">hapus</a>
                    
                    
                        <a href="<?= base_url(); ?>pagination/ubah/<?= $mhs['id']; ?>"
                        class="badge badge-success float-right">ubah</a>
                    
                    
                        <a href="<?= base_url(); ?>pagination/detail/<?= $mhs['id']; ?>"
                        class="badge badge-primary float-right">detail</a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>

            <?= $this->pagination->create_links() ?>

        </div>
        
    </div>

</div>