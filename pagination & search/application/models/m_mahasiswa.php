<?php 

// require 'vendor/autoload.php';

use GuzzleHttp\Client;
use phpDocumentor\Reflection\Types\False_;

class M_mahasiswa extends CI_model {

    private $_client;

    public function __construct(){
        $this->_client = new Client([
            'base_uri' => 'http://localhost/rest-api/codeigniter/api/',
            'auth' => ['ilham', 'ilham123'],
            'LIMIT' => 8,0
        ]);
    }

    public function getAllMahasiswa()
    {
        // return $this->db->get('mahasiswa')->result_array();
        $client = new Client();
        $response = $this->_client->request('GET', 'mahasiswa', [
            'query' => [
                'wpu-key' => 'rahasia'
            ]
        ]);

        $result = json_decode($response->getBody()->getContents(), true);

        return $result['data'];
    }

    public function getMahasiswaById($id)
    {
        $response = $this->_client->request('GET', 'mahasiswa', [
            'query' => [
                'wpu-key' => 'rahasia',
                'id' => $id
            ]
        ]);

        $result = json_decode($response->getBody()->getContents(), true);

        return $result['data'][0];
    }

    public function tambahDataMahasiswa() 
    {
        $data = [
            "name" => $this->input->post('name', true),
            "address" => $this->input->post('address', true),
            "email" => $this->input->post('email', true),
            "wpu-key" => 'rahasia'
        ];

        $response = $this->_client->request('POST', 'mahasiswa', [
            'form_params' => $data
        ]);

        $result = json_decode($response->getBody()->getContents(), true);
        return $result;
    }

    public function hapusDataMahasiswa($id)
    {
        $response = $this->_client->request('DELETE', 'mahasiswa', [
            'form_params' => [
                'id' => $id,
                'wpu-key' => 'rahasia'
            ]
        ]);
        $result = json_decode($response->getBody()->getContents(), true);
        return $result;
    }

    public function ubahDataMahasiswa()
    {
        $data = [
            "name" => $this->input->post('name', true),
            "address" => $this->input->post('address', true),
            "email" => $this->input->post('email', true),
            "id" => $this->input->post('id', true),
            "wpu-key" => 'rahasia'
        ];

        $response = $this->_client->request('PUT', 'mahasiswa', [
            'form_params' => $data
        ]);

        $result = json_decode($response->getBody()->getContents(), true);
        return $result;
    }

    public function cariDataMahasiswa()
    {
        $keyword = $this->input->post('keyword', true);
        $this->db->like('nama', $keyword);
        $this->db->or_like('jurusan', $keyword);
        $this->db->or_like('nrp', $keyword);
        $this->db->or_like('email', $keyword);
        return $this->db->get('mahasiswa')->result_array();
    }

    public function getAllPeoples(){
        $client = new Client();
        $response = $this->_client->request('GET', 'mahasiswa', [
            'query' => [
                'wpu-key' => 'rahasia'
            ]
        ]);

        $result = json_decode($response->getBody()->getContents(), true);

        return $result['data'];

        // return $this->db->get('people')->result_array();
    }

    public function getPeoples($limit, $start, $keyword = null){
        if($keyword){
            $this->db->like('name', $keyword);
        }
        return $this->db->get('people', $limit, $start)->result_array();
    }

    public function countAllPeoples(){
        $client = new Client();
        $response = $this->_client->request('GET', 'mahasiswa'
        );

        $result = json_decode($response->getBody()->getContents(),true);

        return count($result['data']);
    }

}