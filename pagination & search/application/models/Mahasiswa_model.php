<?php

class Mahasiswa_model extends CI_Model{
    public function getMahasiswa($id = null){
        if($id === null){
            return $this->db->get('people')->result_array();
        }else{
            return $this->db->get_where('people', ['id' => $id])->result_array();
        }
    }

    public function deleteMahasiswa($id){
        $this->db->delete('people', ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function createMahasiswa($data){
        $this->db->insert('people', $data);
        return $this->db->affected_rows();
    }

    public function updateMahasiswa($data, $id){
        $this->db->update('people', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function getMahasiswaByNama($keyword){
        $this->db->select('*');
        $this->db->from('mahasiswa');
        $this->db->like('nama', $keyword);

        return $this->db->get()->result();
    }
}