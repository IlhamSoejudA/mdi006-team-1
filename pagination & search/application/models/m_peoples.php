<?php

use GuzzleHttp\Client;

class M_peoples extends CI_Model{

    private $_client;

    public function __construct(){
        $this->_client = new Client([
            'base_uri' => 'http://localhost/rest-api/codeigniter/api/',
            'auth' => ['ilham', 'ilham123']
        ]);
    }

    public function getAllPeoples(){
        // $client = new Client();
        // $response = $this->_client->request('GET', 'mahasiswa', [
        //     'query' => [
        //         'wpu-key' => 'rahasia'
        //     ]
        // ]);

        // $result = json_decode($response->getBody()->getContents(), true);

        // return $result['data'];

        // return $this->db->get('people')->result_array();
    }

    public function getPeoples($limit, $start, $keyword = null){
        if($keyword){
            $this->db->like('name', $keyword);
        }
        return $this->db->get('people', $limit, $start)->result_array();
    }

    public function countAllPeople(){
        return $this->db->get('people')->num_rows();
    }

}